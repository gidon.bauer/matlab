clear; clc; close('all');


x = 0:(2*pi)/1000:2*pi;
y = f(x);

si = size(y);

for i = 1:si(2)
    y(i) = y(i) + random('uniform',0,0.1);
end

p = 0.9;

for i = 2:si(2)
    y(i) = p*y(i-1) + (1-p)*y(i);
end

plot(x,y);

function [y] = f(x)
    y = sin(x);
end

function [y] = g(x) 
    y = 0.001*(x.^3 + 3*x.^2 - 4*x);
end

function [y] = h(x)
    y = 0.001 .* exp(x);
end