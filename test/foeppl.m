clear; clc; close 'all';

x = 0:10e-2:10;
y = -380 + 160*x -100*foep(x,2,1) - 300*foep(x,5,0) - 20*foep(x,7,2) + (20/9)*foep(x,7,3);

plot(x,y);

function [out] = foep(x,a,n)
    si = size(x);
    si = si(2);
    out = zeros(1,si);
    for i = 1:si
        if x(i)>a
            out(i) = (x(i)-a)^n;
%         else
%             out(i) = 0;
        end
    end
end
