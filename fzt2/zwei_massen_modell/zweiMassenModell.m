clear; clc; close all;

%% Lösen der ODEs
t_span = [0;1];
y_0 = [0.01, 0.5, 0.1, 1];

[t, z] = ode45(@zwei_massen_modell, t_span, y_0);
[t_red, z_red] = ode45(@zwei_massen_modell_red, t_span, y_0);

%% Plotten - Zwei Massen Ersatzmodell
figure(1);
hold on;
for i = 1:4
    plot(t, z(:,i));
end
title('Zwei Massen Ersatzmodell');
legend('$z_A$', '$\dot z_A$', '$z_R$', '$\dot z_R$', 'Interpreter', 'latex');
hold off;

%% Plotten - Vereinfachtes Zwei Massen Ersatzmodell
figure(2);
hold on;
for i = 1:4
    plot(t_red, z_red(:,i));
end
title('Vereinfachtes Zwei Massen Ersatzmodell');
legend('$z_A$', '$\dot z_A$', '$z_R$', '$\dot z_R$', 'Interpreter', 'latex');
hold off;

%% Plotten Vergleich
figure(3);
tiledlayout(2,2);

nexttile;
hold on;
plot(t, z(:,1));
plot(t_red, z_red(:,1));
xlabel('$t$ [$s$]', 'Interpreter', 'latex');
ylabel('$z_A$ [$\frac{m}{s}$]', 'Interpreter', 'latex');
title('$z_A$', 'Interpreter', 'latex');
legend('real', 'vereinfacht');
hold off;

nexttile;
hold on;
plot(t, z(:,2));
plot(t_red, z_red(:,2));
xlabel('$t$ [$s$]', 'Interpreter', 'latex');
ylabel('$\dot z_A$ [$\frac{m}{s^2}$]', 'Interpreter', 'latex');
title('$\dot z_A$', 'Interpreter', 'latex');
legend('real', 'vereinfacht');
hold off;

nexttile;
hold on;
plot(t, z(:,3));
plot(t_red, z_red(:,3));
xlabel('$t$ [$s$]', 'Interpreter', 'latex');
ylabel('$z_R$ [$\frac{m}{s}$]', 'Interpreter', 'latex');
title('$z_R$', 'Interpreter', 'latex');
legend('real', 'vereinfacht');
hold off;

nexttile;
hold on;
plot(t, z(:,4));
plot(t_red, z_red(:,4));
xlabel('$t$ [$s$]', 'Interpreter', 'latex');
ylabel('$\dot z_R$ [$\frac{m}{s^2}$]', 'Interpreter', 'latex');
title('$\dot z_R$', 'Interpreter', 'latex');
legend('real', 'vereinfacht');
hold off;

%% Zwei Massen Ersatzmodell
function [z] = zwei_massen_modell(~, y)
    % Konstanten
    m_1 = 40;    % [kg]
    m_2 = 400;   % [kg]
    k_A = 1500;  % [Ns/m]
    c_A = 2.1e4; % [N/m]
    k_R = 100;   % [Ns/m]
    c_R = 1.5e5; % [N/m]
    
    z = zeros(4,1);
    z(1) = y(2);
    z(2) = -(k_A / m_2) * (y(2) - y(4)) - (c_A / m_2) * (y(1) - y(3));
    z(3) = y(4);
    z(4) = (k_A / m_1) * (y(2) - y(4)) - (k_R / m_1) * y(4) ...
           + (c_A / m_1) * (y(1) - y(3)) - (c_R / m_1) * y(3);
end

%% Vereinfachtes Zwei Massen Ersatzmodell
function [z] = zwei_massen_modell_red(~, y)
    % Konstanten
    m_1 = 40;    % [kg]
    m_2 = 400;   % [kg]
    k_A = 1500;  % [Ns/m]
    c_A = 2.1e4; % [N/m]
    k_R = 100;   % [Ns/m]
    c_R = 1.5e5; % [N/m]
    
    z = zeros(4,1);
    z(1) = y(2);
    z(2) = -(k_A / m_2) * y(2) - (c_A / m_2) * y(1);
    z(3) = y(4);
    z(4) = -((k_A + k_R) / m_1) * y(4) - ((c_A + c_R) / m_1) * y(3);
end