%% Mathe III - Programmieraufgabe 04
%% Gidon Bauer & Michel Mantsch

%% Clear the workspace
clear; clc; close all;

%% Define Matrices and Epsilon
A = [ 2,-3, 1;
      3, 1, 3;
     -5, 2,-4];
 
B = [3.5488, 15.593, 8.5775, -4.0123;
      2.3595, 24.524, 14.596, -5.8157;
      0.0899, 27.599, 21.438, -5.8415;
      1.9227, 55.667, 39.717, -10.558];

size_C = 5;
C = zeros(size_C);
for i = 1:size_C
    for j = 1:size_C
        C(i,j) = sin(i * j);
    end
end

eps = 1e-8;

%% Choose which part you want to test (1 to 4)
choice = 2;

%% a)
if choice == 1
    % Eigenvalues of Matrix A
    partA(A, eps, 'Matrix A');
    % Eigenvalues of Matrix B
    partA(B, eps, 'Matrix B');    
    % Eigenvalues of Matrix C
    partA(C, eps, 'Matrix C');

%% b)
elseif choice == 2
    % Matrix A
    partB(A, eps, 1, 'Matrix A');

    % Matrix B
    partB(B, eps, 2, 'Matrix B');
    
    % Matrix C
    partB(C, eps, 3, 'Matrix C');
    
%% c)
elseif choice == 3
    % Matrix A
    partC(A, eps, 1, 'Matrix A');
    
    % Matrix B
    partC(B, eps, 2, 'Matrix B');
    
    % Matrix C
    partC(C, eps, 3, 'Matrix C');
    
%% d)
else
    % Matrix A
    partD(A, eps, 1, 'Matrix A');
    
    % Matrix B
    partD(B, eps, 3, 'Matrix B');

    % Matrix C
    partD(C, eps, 5, 'Matrix C');    
end

%% Tidy up
clear i; clear j; clear choice;

%% Part A as a function
function [] = partA(A, eps, ti)
    A_eig_M = eig(A);
    
    [~, ~, Ak_without] = QRwithoutShift(A, eps);
    [~, ~, l_A_without] = size(Ak_without);
    A_eig_without = diag(Ak_without(:, :, l_A_without));
    
    [~, ~, Ak_with] = QRwithShift(A, eps);
    [~, ~, l_A_with] = size(Ak_with);
    A_eig_with = diag(Ak_with(:, :, l_A_with));
    
    disp(ti);
    disp(A_eig_M);
    disp(A_eig_without);
    disp(A_eig_with);
end

%% Part B as a function
function [] = partB(A, eps, fig, ti)
    [A_without_step, A_without_error, ~] = QRwithoutShift(A, eps);
    [A_with_step, A_with_error, ~] = QRwithShift(A, eps);
    figure(fig);
    semilogy(A_without_step, A_without_error);
    hold on;
    semilogy(A_with_step, A_with_error);
    xlabel('Steps');
    ylabel('Error');
    title(ti);
    legend('Without Shift', 'With Shift');
    hold off;
end

%% Part C as a function
function [] = partC(A, eps, fig, ti)
    A_eig_M = eig(A);
    max_eig_M = max(abs(A_eig_M));

    [A_without_step, ~, Ak_without] = QRwithoutShift(A, eps);
    A_err_without = zeros(size(A_without_step));
    [~, si_A_without] = size(A_without_step);
    for i = 1:si_A_without
        max_eig_without = max(abs(diag(Ak_without(:, :, i))));
        A_err_without(i) = abs(max_eig_M - max_eig_without) / max_eig_M;
    end
    
    [A_with_step, ~, Ak_with] = QRwithShift(A, eps);
    A_err_with = zeros(size(A_with_step));
    [~, si_A_with] = size(A_with_step);
    for i = 1:si_A_with
        max_eig_with = max(abs(diag(Ak_with(:, :, i))));
        A_err_with(i) = abs(max_eig_M - max_eig_with) / max_eig_M;
    end
    
    figure(fig);
    semilogy(A_without_step, A_err_without);
    hold on;
    semilogy(A_with_step, A_err_with);
    xlabel('Steps');
    ylabel('Relative Error of max. Eigenvalue');
    title(ti);
    legend('Without Shift', 'With Shift');
    hold off;
end

%% Part D as a function
function [] = partD(A, eps, fig, ti)
    colormap(flipud(gray));
    [A_without_step, ~, Ak_without] = QRwithoutShift(A, eps);
    [~, A_without_si] = size(A_without_step);

    figure(fig);
    imagesc(log(abs(Ak_without(:, :, (A_without_si-2):A_without_si)))); % Plot with the last three steps
%     imagesc(log(abs(Ak_without(:, :, A_without_si)))); % Plot with only the last step
    colorbar;
    drawnow;
    title(join([ti, ' without Shift']));

    [A_with_step, ~, Ak_with] = QRwithShift(A, eps);
    [~, A_with_si] = size(A_with_step);

    figure(fig+1);
    imagesc(log(abs(Ak_with(:, :, (A_with_si-2):A_with_si)))); % Plot with the last three steps
%     imagesc(log(abs(Ak_with(:, :, A_with_si)))); % Plot with only the last step
    colorbar;
    drawnow;
    title(join([ti, ' with Shift']));
end

%% QR Method without Shift
function [step, error, Ak] = QRwithoutShift(A, eps)
    Ak(:, :, 1) = hess(A);
    [n, ~] = size(A);
    error(1) = 0;
    step(1) = 1;
    for i = 2:n
        error(1) = error(1) + abs(Ak(i, i-1, 1));
    end
    j = 2;
    while error(j-1) > eps
        [Q, R] = qr(Ak(:, :, j-1));
        Ak(:, :, j) = R * Q;
        error(j) = 0;
        for i = 2:n
            error(j) = error(j) + abs(Ak(i, i-1, j));
        end
        step(j) = j;
        j = j + 1;
    end
end

function [step, error, Ak] = QRwithShift(A, eps)
    Ak(:, :, 1) = hess(A);
    [n, ~] = size(A);
    error(1) = 0;
    step(1) = 1;
    for i = 2:n
        error(1) = error(1) + abs(Ak(i, i-1, 1));
    end
    mue = Ak(n, n, 1);
    j = 2;
    while error(j-1) > eps
        [Q, R] = qr(Ak(:, :, j-1) - mue * eye(n, n));
        Ak(:, :, j) = R * Q + mue * eye(n, n);
        error(j) = 0;
        for i = 2:n
            error(j) = error(j) + abs(Ak(i, i-1, j));
        end
        step(j) = j;
        j = j + 1;
    end
end