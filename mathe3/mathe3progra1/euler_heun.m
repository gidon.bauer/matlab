%% Mathe III Programmieraufgabe 1
%% Michel Mantsch & Gidon Bauer

%% Clear workspace
clear; clc; close all;

%% Testing the function
%% Define Parameters
example_n = 40;
max_n = 5000;
t_0 = 0;
t_end = 3;
x_0 = 1;

%% Choose
choice = 1;

if choice == 1
    %% Calculate one specific solution
    [x_e,t_e] = euler_method(@f, example_n, t_0, t_end, x_0);
    [x_h,t_h] = heun_method(@f, example_n, t_0, t_end, x_0);
    t_a = 0:0.01:t_end; t_a = t_a';
    x_a = analytic_solution(t_a);
    
    %% Plot that solution
    figure(1);
    plot(t_e, x_e, 'g');
    hold on;
    plot(t_h, x_h, 'r');
    plot(t_a, x_a, 'b--');
    title("Solution of initial value problem for one specific n");
    xlabel("t");
    ylabel("x");
    hold off;
elseif choice == 2
    %% Calculate error for differnt n
    n = 1:max_n; n = n';
    err_e = zeros(size(n));
    err_h = zeros(size(n));
    for numberPoints = 1:max_n
        [x_e,t_e] = euler_method(@f, numberPoints, t_0, t_end, x_0);
        [x_h,t_h] = heun_method(@f, numberPoints, t_0, t_end, x_0);
        x_a = analytic_solution(t_e);
        err_e(numberPoints) = error(x_a, x_e);
        err_h(numberPoints) = error(x_a, x_h);
        %     disp("Number of points: ");
        %     disp(numberPoints);
    end
    
    %% Plot error against number of points
    figure(2);
    order_1 = 1./n;
    order_2 = order_1./n;
    loglog(n, err_e, 'g');
    hold on;
    loglog(n, err_h, 'r');
    loglog(n, order_1, 'b');
    loglog(n, order_2, 'b');
    title("Error");
    xlabel("n");
    ylabel("Error");
    hold off;
end
%% tidy up the workspace
clear t_0, clear t_end; clear x_0; clear numberPoints; clear example_n; clear max_n; clear choice;

%% test function
function [y] = f(t,x)
    y = 3*exp(-4*t) - 2*x;
end

%% Analytic solution 
function [y] = analytic_solution(x)
    y = (5/2) * exp(-2.*x) - (3/2) * exp(-4.*x);
end

%% Euler method
function [x,t] = euler_method(func, n, t_0, t_end, x_0)
    t = zeros(n,1);
    x = zeros(n,1);
    h = (t_end-t_0)/n;
    t(1) = t_0;
    x(1) = x_0;
    for k = 2:n
        t(k) = t_0 + (k-1) * h;
        x(k) = x(k-1) + h*func(t(k-1), x(k-1));
    end
end

%% Heun method
function [x,t] = heun_method(func, n, t_0, t_end, x_0)
    t = zeros(n,1);
    x = zeros(n,1);
    h = (t_end-t_0)/n;
    t(1) = t_0;
    x(1) = x_0;
    for k = 2:n
        t(k) = t_0 + (k-1) * h;
        x_p = x(k-1) + h*func(t(k-1),x(k-1));
        x(k) = x(k-1)+ (h/2)*(func(t(k-1),x(k-1)) + func(t(k),x_p));
    end
end

%% Error function
function [err] = error(analytic_solution, numerical_solution)
    err = norm(numerical_solution-analytic_solution, inf);
end