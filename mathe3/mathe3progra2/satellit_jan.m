choice = 1;

%% Aufgabenteil a)
if choice == 1
options.InitialStep = 0.1;
options.SafetyFactor = 0.5;
options.AbsTol = 0.01;
f = @ode;
tspan = [0,1];
x0 = 1; 
[t,x] = RKutta23(f, tspan, x0, options);
plot(t,x)
hold on
plot (t,exp(-10*t))
hold off
end

%% Aufgabenteil c)
if(choice == 2)
options.InitialStep = 0.1;
options.SafetyFactor = 0.5;
options.AbsTol = 0.001;
f = @satellite_ode;
tspan = [0,17.06521656015796];
x0 = [0.994;0;0;-2.00158510637608]; 
[t,x] = RKutta23(f, tspan, x0, options);
plot(x(1),x(2))
end
%% Testfunktion
function [y] = ode(t,x)
    y = -10*x;
end

%% Sattelite-ODE
function [y] = satellite_ode(t,x)
    my = 0.012277471;
    y = zeros(4,1);
    my_dach = 1 - my;
    N1 = ((x(1)+my)^2+x(2)^2)^(3/2);
    N2 = ((x(1)-my_dach)^2+x(2)^2)^(3/2);
    y(1) = x(3);
    y(2) = x(4);
    y(3) = x(1)+2*x(4)-my_dach*(x(1)+my)/N1-my*(x(1)-my_dach)/N2;
    y(4) = x(2)-2*x(3)-my_dach*x(2)/N1-my*x(2)/N2;
end

%% Runge-Kutta-Verfahren 2
function [x_new] = verfahren_2(f,h,t,x)
    k_1 = f(t,x);
    k_2 = f(t+h,x+f(t,x));
    k_3 = f(t+h/2,x+(1/4)*(f(t,x)+f(t+h,x+f(t,x))));
    x_new = (1/6)*k_1+(1/6)*k_2+(4/6)*k_3;
end

%% Runge-Kutta-Verfahren 1
function [x_new] = verfahren_1(f,h,t,x)
    k_1 = f(t,x);
    k_2 = f(t+h,x+f(t,x));
    k_3 = f(t+h/2,x+(1/4)*(f(t,x)+f(t+h,x+f(t,x))));
    x_new = (1/2)*k_1+(1/2)*k_2+0*k_3;
end

%% Runge-Kutta-Verfahren mit adaptiver Schrittweitensteuerung
function [t,x] = RKutta23(f, tspan, x0, options)
    h = options.InitialStep;
    i = 1;
    x(:,i) = x0;
    t(i) = tspan(1);
    while t(i)<tspan(2)
        t(i+1) = t(i)+h;
        x_v1 = x(:,i) + h*verfahren_1(f,h,t(i),x(:,i));
        x_v2 = x(:,i) + h*verfahren_2(f,h,t(i),x(:,i));
        eps = norm(x_v1-x_v2,inf);
        if eps < options.AbsTol
            x(:,i+1) = x_v2;
            h = h*sqrt(options.SafetyFactor*options.AbsTol/eps);
        else
            while eps >= options.AbsTol
                h = h*sqrt(options.SafetyFactor*options.AbsTol/eps);
                x_v1 = x(:,i) + h*verfahren_1(f,h,t(i),x(:,i));
                x_v2 = x(:,i) + h*verfahren_2(f,h,t(i),x(:,i));
                eps = norm(x_v1-x_v2,inf);
            end
            x(:,i+1) = x_v1;
        end
        i = i + 1;
    end
end

