%% Mathe III - Programmieraufgabe 02
%% Gidon Bauer & Michel Mantsch

%% Clear Workspace
clear; clc; close all;

%% Choose what you want to test
choice = 2;

%% Test the programm with the exponetial function
if choice == 1
    %% Define Parameters
    x_0 = 1;
    t_span = [0; 1];
    options.InitialStep = 1e-1;
    options.SafetyFactor = 0.5; % from (0,1]
    options.AbsTol = 0.3;
    %% Run RK23
    % With SafetyFactor = 0.3
    [t_highSF,x_highSF] = RKutta23(@exponential_ode, t_span, x_0, options);
    plot(t_highSF,x_highSF,'r');
    % With SaftyFactor = 0.01
    options.AbsTol = 0.01;
    [t_lowSF,x_lowSF] = RKutta23(@exponential_ode, t_span, x_0, options);
    hold on
    plot(t_lowSF,x_lowSF,'b');
    % Analytical Solution
    t_a = 0:0.01:1;
    plot(t_a,exp(-10*t_a),'g--');
    xlabel('t');
    ylabel('x');
    title('Numerical and analyitical plot of e^-^1^0^t');
    hold off
    
%% Test the programm with the satellite
elseif choice == 2
    %% Define Parameters
    x_0 = [0.994; 0; 0; -2.001585106376082];
    t_span = [0; 17.06521656015796];
    options.InitialStep = 1e-1;
    options.SafetyFactor = 0.5; % from (0,1]
    options.AbsTol = 0.001;
    %% Run RK23
    [t,x] = RKutta23(@satellite_ode, t_span, x_0, options);
    figure(1);
    % Plot the path
    plot(x(1,:),x(2,:));
    xlabel('x(1)');
    ylabel('x(2)');
    title('Path of the satellite');
    % plot delta t over steps
    steps = 1:1:length(t);
    delta_t = zeros(1,length(t));
    for k = 1:length(t)-1
        delta_t(k) = t(k+1) - t(k);
    end
    figure(2);
    plot(steps,delta_t);
    xlabel('steps');
    ylabel('delta t');
    title('Increment of t');
end

%% Tidy up the workspace
clear choice; clear t_a; clear k;

%% Exponential function as ODE
function [y] = exponential_ode(~,x)
    y = -10 * x;
end

%% Movement of the satellite
function [y] = satellite_ode(~, x)
    mue = 0.012277471;
    y = zeros(4,1);
    N_1 = ((x(1) + mue)^2 + x(2)^2)^(3/2);
    N_2 = ((x(1) - (1-mue))^2 + x(2)^2)^(3/2);

    y(1) = x(3);
    y(2) = x(4);
    y(3) = x(1) + 2*x(4) - (1-mue) * (x(1)+mue)/N_1 - mue * (x(1)-(1-mue))/N_2;
    y(4) = x(2) - 2*x(3) - (1-mue) * x(2)/N_1 - mue * x(2)/N_2;
end

%% Runge Kutta Method of order 2
function [y] = RK2(func, h, t, x)
    k1 = func(t, x);
    k2= func(t + h, x + h*k1);
    y = x + h*(k1/2 + k2/2);
end
%% Runge Kutta Method of order 3
function [y] = RK3(func, h, t, x)
    k1 = func(t, x);
    k2= func(t + h, x + h*k1);
    k3 = func(t + h/2, x + h*(k1/4 + k2/4));    
    y = x + h*(k1/6 + k2/6 + 4*k3/6);
end
%% RK23
function [t,y] = RKutta23(func, tspan, x_0, options)
    y = [];
    t = [];
    y(:,1) = x_0;
    t(1) = tspan(1);
    h = options.InitialStep;
    i = 2;
    while t(i-1) < tspan(2)
        y_2 = RK2(func, h, t(i-1)+h, y(:,i-1));
        y_3 = RK3(func, h, t(i-1)+h, y(:,i-1));
        eps = norm(y_3-y_2, inf);
        if eps < options.AbsTol
            h = h*sqrt(options.SafetyFactor*options.AbsTol/eps);
            if t(i-1)+h > tspan(2)
                h = tspan(2) - t(i-1);
            end
            y(:,i) = y_3;
        else
            while eps >= options.AbsTol
                h = h*sqrt(options.SafetyFactor*options.AbsTol/eps);
                if t(i-1)+h > tspan(2)
                    h = tspan(2) - t(i-1);
                end
                y_2 = RK2(func, h, t(i-1)+h, y(:,i-1));
                y_3 = RK3(func, h, t(i-1)+h, y(:,i-1));
                eps = norm(y_3-y_2, inf);
            end
            y(:,i) = y_2;
        end
        t(i) = t(i-1) + h;
        i = i + 1;
    end 
end
