%% Mathe III - Programmieraufgabe 03
%% Gidon Bauer & Michel Mantsch

%% Clear the workspace
clear; clc; close all;

%% Define Parameters
tspan = [0;10];
x0 = [10;5];
steps = 100;

%% Calculate the solution with the implicit Runge-Kutta method
[t_rk4_imp, x_rk4_imp] = RK4_implicit(@lv_ode, @jacobi_lv, tspan, x0, steps);

%% Calculate the solution with the MATLAB function ode15
[t_ode15, x_ode15] = ode15s(@lv_ode, tspan, x0);

%% Plot the solutions
figure(1);
plot(x_rk4_imp(:,1), x_rk4_imp(:,2));
hold on;
% plot(x_ode15(:,1), x_ode15(:,2));
hold off;

figure(2);
plot(t_rk4_imp, x_rk4_imp);
hold on;
% plot(t_ode15, x_ode15);
hold off;

%% Lotka-Volterra ODE
function [y] = lv_ode(~,x)
    c = [1;0.5];
    d = [1/10;1/15];
    y = zeros(2,1);
    y(1) = c(1) * (1 - d(1)*x(2)) * x(1);
    y(2) = c(2) * (d(2)*x(1) - 1) * x(2);
end

%% Jacobian of the Lotka-Volterra equations
function [y] = jacobi_lv(~,x)
    c = [1;0.5];
    d = [1/10;1/15];
    y = zeros(2,2);
    y(1,1) = c(1)*(1-d(1)*x(2));
    y(1,2) = -c(1)*d(1)*x(1);
    y(2,1) = c(2)*d(2)+x(2);
    y(2,2) = c(2)*(d(2)*x(1)-1);
end

%% Version of the newton method for NST
function [k1, k2] = newton(func, jacobian, maxit, tol, t, x, h, k1, k2)
%     iter = 0;
    for i = 1:maxit
        k1 = k1 - (DF1(jacobian, t, x, h, k1, k2)) \ (F1(func, t, x, h, k1, k2));
        k2 = k2 - (DF2(jacobian, t, x, h, k1, k2)) \ (F2(func, t, x, h, k1, k2));
%         iter = iter + 1;
        if (F1(func, t, x, h, k1, k2) < tol) & (F2(func, t, x, h, k1, k2) < tol)
            return;
        end
    end
end

%% F1 - Helperfunction
function [y] = F1(func, t, x, h, k1, k2)
    y = k1 - func(t + h*(0.5 - (sqrt(3)/6)), x + h*(k1/4 + (0.25 -(sqrt(3)/6))*k2));
end

function [y] = DF1(jacobian, t, x, h, k1, k2)
    y = ones(2,2) - jacobian(t + h*(0.5 - (sqrt(3)/6)), x + h*(k1/4 + (0.25 -(sqrt(3)/6))*k2) * h*((0.25 - (sqrt(3)/6)) + 0.25));
end

%% F2 - Helperfunction
function [y] = F2(func, t, x, h, k1, k2)
    y = k2 - func(t + h*(0.5 + (sqrt(3)/6)), x + h*((0.25 +(sqrt(3)/6))*k1) + k2/4); 
end

function [y] = DF2(jacobian, t, x, h, k1, k2)
    y = ones(2,2) - jacobian(t + h*(0.5 + (sqrt(3)/6)), x + h*((0.25 +(sqrt(3)/6))*k1) + k2/4) * h*((-(0.25 -(sqrt(3)/6))) + 0.25);
end

%% Implicit Runge-Kutta method of order 4
function [t,y] = RK4_implicit(func, jacobian, tspan, x0, steps)
    h = (tspan(2) - tspan(1))/steps;
    t = zeros(steps, 1);
    t(1) = tspan(1);
    si = size(x0);
    y = zeros(steps, si(1));
    y(1, :) = x0;
    for i = 2:steps
        t(i) = t(i-1) + h;
%         k1 = func(t(i), y(i-1,:));
%         k2 = func(t(i), y(i-1,:));
        k1 = [1;1];
        k2 = [1;1];
        [k1, k2] = newton(func, jacobian, 1e4, 1e-4, t(i), y(i,:), h, k1, k2);
        y(i,:) = y(i-1,:) + h * (k1'/2 + k2'/2);
    end
end