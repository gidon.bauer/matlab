clc; clear;

A = [9 0 0 0; -8 2 0 4; 7 7/2 -5 7/2; 6 0 0 6];
x_0 = 0.5 * [1;1;1;1];

% [EW, EV] = fwd_it(A,x_0,3);
[EW, EV] = bwd_it_guess(A,x_0,3,0);
% [EW, EV] = bwd_it_guess(A,x_0,3,5);

disp(EW);
disp(EV);

function [EW, EV] = fwd_it(A,x,n)
    for i = 1:n
        x = A*x/norm(A*x,2);
    end
    EV = x;
    EW = (x'*(A*x))/(x'*x);
end

function [EW, EV] = bwd_it_guess(A,x,n,EW_guess)
    for i = 1:n
        x = ((A-EW_guess*eye(size(A)))\x)/norm((A-EW_guess*eye(size(A)))\x,2);
    end
    EV = x;
    EW = (x'*(A*x))/(x'*x);
end