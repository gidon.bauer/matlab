%% Mathe III - Hausaufgabe 14
%% Michel Mantsch (391099) & Gidon Bauer (392617)

%% Clear workspace
clear; clc; close all;

%% Choose 1 for a) and 2 for b)
choice = 2;

%% a)
if choice == 1
    maxit = 10000;
    eps = 1e-3;
    x0 = [-1.2;1];
    [min, steps] = Steepest(@Rosenbrock, @gradRosenbrock, x0, eps, maxit);
    disp(steps);
    disp(min);

%% b)
elseif choice == 2
    maxit = 10000;
    eps = 1e-3;
    x0 = [-10;30];
    [min,steps] = Steepest(@g, @gradG, x0, eps, maxit);
    disp(steps);
    disp(min);
end

%% Tidy up the workspace
clear choice;

%% Rosenbrockfunction
function[y] = Rosenbrock(x)
    a = 100; % Change to 1000 to test the function
    y = a * (x(2) - x(1)^2)^2 + (1 - x(1))^2;
end

%% Gradient of the Rosenbrockfunction
function[y] = gradRosenbrock(x)
    a = 100; % Change to 1000 to test the function
    y = zeros(2,1);
    y(1) = -4 * a * x(1) * (x(2) - x(1)^2) - 2 * (1 - x(1));
    y(2) = 2 * a * (x(2) - x(1)^2);
end

%% function g from b)
function[y] = g(x)
    y = x(1)^2 + (x(1)^2+1)*(x(2)^2-1)^2;
end

%% Gradient of g(x)
function[y] = gradG(x)
    y = zeros(2,1);
    y(1) = 2*x(1) + 2*x(1)*(x(2)^2-1)^2;
    y(2) = 4*x(2)*(x(1)^2+1)*(x(2)^2-1);
end


%% function computes the length of a step
function[s] = Armijo(f,gf,x)
    beta = 0.5;
    gamma = 10^(-4);
    r = -gf(x);
    s = 1;
    while (f(x + s*r) - f(x)) > (s * gamma * (gf(x)') * r)
        s = s * beta;
    end
end

%% function finds the minimum of f
function[x,steps] = Steepest(f,gf,x0,eps,maxit)
    x = x0;
    steps = 0;
    while (abs(norm(gf(x),2)) > eps) && (steps < maxit) 
        s = Armijo(f, gf, x);
        r = -gf(x);
        x = x + s*r;
        steps = steps + 1;
    end
end