%% Calculates the mean initial condition
function [init_mean] = mean_init_cond(func, xspan, N)
    dx = (xspan(2) - xspan(1))/(N+1);

    init_mean = zeros(N,1);
    for i = 1:N
        init_mean(i) = 1/dx * integral(func, xspan(1) + (i - 0.5)*dx, xspan(1) + (i+0.5)*dx);
    end
end