%% Lax-Wendroff Flux function
function [y] = lax_wendroff_flux(ul, ur, f, dt, dx)
    if (ul ~= ur)
        a = ((f(ul) - f(ur))/(ul - ur)).^2;
    else
        a = 0;
    end
    
    y = (1/2)*(f(ul) + f(ur)) - (dt/(2*dx)) * (ur - ul) * a;
end