%% Periodic initial conditions
function [y] = step_func(x)
    y = ones(size(x));
    
    for i = 1:length(x)
        if (x(i) >= -0.5 && x(i) < 0.5)
            y(i) = 0;
        end
    end
end