%% Calculate the max. absolut derivative
function [m] = max_der(func, u, N)
    umax = max(u);
    umin = min(u);
    
    eps = (umax - umin)/N;
    
    uline = umin:eps:umax;
    f_der = abs(1/eps * diff(func(uline)));
    
    m_ = max(f_der);
    if m_ > 1e5
        m = 1e5;
    else
        m = m_;
    end
end