%% Finite Volume method
function [u, t] = FVM(u0, flux, f, xspan, tspan, N)
    xsize = length(u0);

    dx = (xspan(2) - xspan(1))/(N-1);
    
    u = zeros(xsize, 1);
    u(:, 1) = u0;

    t = [];
    t(1) = tspan(1);

    n = 1;
    while t(end) < tspan(2)
        %% Calc dt
        dt = dx/max_der(f,u(:,n), 100);

        t(n+1) = t(n) + dt;            
        for j = 1:xsize
            %% Calc next value
            if (j == 1)
                u(j,n+1) = u(j,n) + (dt/dx) * (flux(u(end,n), u(j,n), f, dt, dx) - flux(u(j,n), u(j+1,n), f, dt, dx));
            elseif (j == xsize)
                u(j,n+1) = u(j,n) + (dt/dx) * (flux(u(j-1,n), u(j,n), f, dt, dx) - flux(u(j,n), u(1,n), f, dt, dx));
            else
                u(j,n+1) = u(j,n) + (dt/dx) * (flux(u(j-1,n), u(j,n), f, dt, dx) - flux(u(j,n), u(j+1,n), f, dt, dx));
            end
        end
        n = n + 1;
    end
end