%% Mathe V - Progammieraufgabe 03
% Stefan Basermann, Adrien Bordes & Gidon Bauer

%% Clear the workspace
clear; clc; close all;

%% Choose the part you want to test
choice = input('Choose which part you want to test. (1-2) ');

%% Functions for conservation law
% Advection
fa = @(u) 2 * u;

% Burgers
fb = @(u) (1/2) * u.^2;

%% Fluxfunctions
% Naive
F1 = @(ul, ur, f, ~, ~) (1/2)*(f(ul) + f(ur));

% Lax-Friedrichs
F2 = @(ul, ur, f, dt, dx) (1/2)*(f(ul) + f(ur)) - (dx/(2*dt)) * (ur - ul);

% Lax-Wendroff
F3 = @lax_wendroff_flux;

%% 1)
if choice == 1
    %% Setup
    N = 100;
    
    tspan = [0; 1];
    xspan = [-1; 1];

    x = linspace(xspan(1), xspan(2), N);

    u0 = mean_init_cond(@step_func, xspan, N);
%     u0 = mean_init_cond(@(x) sin(pi*x), xspan, N);

    %% Solving the advection equation
    [u1a, t1a] = FVM(u0, F1, fa, xspan, tspan, N);
    [u2a, t2a] = FVM(u0, F2, fa, xspan, tspan, N);
    [u3a, t3a] = FVM(u0, F3, fa, xspan, tspan, N);

    figure(1);
    tiledlayout(2,2);
    
    nexttile;
    pcolor(t1a,x,u1a);
    xlabel('t');
    ylabel('x');
    title('Naive');

    nexttile;
    pcolor(t2a,x,u2a);
    xlabel('t');
    ylabel('x');
    title('Lax-Friedrichs');

    nexttile;
    pcolor(t3a,x,u3a);
    xlabel('t');
    ylabel('x');
    title('Lax-Wendroff');


    %% Solving the burges equation
    [u1b, t1b] = FVM(u0, F1, fb, xspan, tspan, N);
    [u2b, t2b] = FVM(u0, F2, fb, xspan, tspan, N);
    [u3b, t3b] = FVM(u0, F3, fb, xspan, tspan, N);

    figure(2);
    tiledlayout(2,2);
    nexttile;
    pcolor(t1b,x,u1b);
    xlabel('t');
    ylabel('x');
    title('Naive');

    nexttile;
    pcolor(t2b,x,u2b);
    xlabel('t');
    ylabel('x');
    title('Lax-Friedrichs');

    nexttile;
    pcolor(t3b,x,u3b);
    xlabel('t');
    ylabel('x');
    title('Lax-Wendroff');

%% 2)
elseif choice == 2
    %% Setup
    N = 200;
    
    tspan = [0; 1];
    t = linspace(tspan(1), tspan(2), N);

    xspan = [-1; 1];
    x = linspace(xspan(1), xspan(2), N);

    %% Exact solution Burgers equation
    u0 = @step_func;
    
    [xmin,u0min] = fminbnd(u0, xspan(1), xspan(2));
    [xmax,u0max] = fminbnd(@(x) -u0(x), xspan(1), xspan(2));
    u0max = -u0max;
    uEx =@(x,t) fminbnd(@(u) (u - u0(x-u*t)).^2, u0min, u0max);

    UEx = zeros(length(x), length(t));
    
    for i = 1:length(x)
        for j = 1:length(t)
            UEx(i,j) = uEx(x(i), t(j));
        end
    end

    figure(3);
    pcolor(t,x,UEx);
    xlabel('t');
    ylabel('x');
    title('Exact');
end


