%% Solves the linear system of equations
function [alpha] = linEqsSolver(A,b)
    alpha = A\b;
end