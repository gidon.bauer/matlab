%% Calculates the right hand side with f(x) = 1
function [b] = rhs_1(grid, n)
    h = (grid(end)-grid(1))/(n+1);
    b = h * ones(n,1);
end