%% Creates equidistant grid
function [g] = grid(x_span, n)
    h = (x_span(2)-x_span(1))/(n+1);
    g = (x_span(1):h:x_span(2))';
end