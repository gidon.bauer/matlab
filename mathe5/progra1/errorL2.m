%% Error analysis using the L2-Norm
function [err, H] = errorL2(n_max, rhs, u_a, x_span)
    H = zeros(n_max, 1);
    err = zeros(n_max, 1);
    counter = 1;
    for n_i = 1:n_max
        g = grid(x_span, n_i);
        A = assembleMatrix(x_span, n_i);
        b = rhs(g, n_i);
        
        alpha = linEqsSolver(A,b);

        err_func = @(x) abs(u_a(x) - bestapprox(x, alpha, n_i, g)).^2;

        H(counter) = (x_span(2)-x_span(1))/(n_i+1);
        err(counter) = sqrt(integral(err_func, x_span(1), x_span(2)));
        counter = counter + 1;
    end
end