%% Bestapproximation
function [y] = bestapprox(x, alpha, n, grid)
    y = 0;
    for i = 1:n
        y = y + alpha(i) * hat_function(i, x, grid);
    end
end