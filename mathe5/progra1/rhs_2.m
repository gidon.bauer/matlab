%% Calculates the right hand side with f(x) = 2*delta(x-0.5)
function [b] = rhs_2(grid, n)
    b = zeros(n,1);
    for i = 1:n
        b(i) = 2 * hat_function(i, 1/2, grid);
    end
end