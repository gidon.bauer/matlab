%% Mathe IV - Programieraufgabe 04
% Michel Mantsch, Stefan Basermann, Adrien Bordes & Gidon Bauer

%% Clear the workspace
clear; clc; close all;

%% Choose which part you want to test (1-2)
choice = input('Choose which part you want to test. (1-2) ');

%% Setup
x_span = [0; 1];

%% 1. & 2.
if choice == 1
    %% Calcuations
    n = 9;
    g = grid(x_span, n);

    A = assembleMatrix(x_span, n);
    b1 = rhs_1(g, n);
    b2 = rhs_2(g, n);

    alpha1 = linEqsSolver(A,b1);
    alpha2 = linEqsSolver(A,b2);

    %% Plotting
    x = (x_span(1):0.001:x_span(2))';

    % Analytical solution for f(x) = 1
    u_a1 = @(x) -(1/2) * (x - (1/2)).^2 + (1/8);
    % Analytical solution for f(x) = 2*delta(x-0.5)
    u_a2 = @(x) -abs(x - (1/2)) + (1/2);
    
    figure(1);
    tiledlayout(2,2);

    nexttile;
    plot(x, bestapprox(x, alpha1, n, g));
    title('FEM - f_1(x) = 1');
    xlabel('x');
    ylabel('u_1(x)');
    
    nexttile;
    plot(x, u_a1(x));
    title('Analytical Solution - f_1(x) = 1');
    xlabel('x');
    ylabel('u_1(x)');
    
    nexttile;
    plot(x, bestapprox(x, alpha2, n, g));
    title('FEM - f_2(x) = 2*delta(x-0.5)');
    xlabel('x');
    ylabel('u_2(x)');
    
    nexttile;
    plot(x, u_a2(x));
    title('Analytical Solution - f_2(x) = 2*delta(x-0.5)');
    xlabel('x');
    ylabel('u_2(x)');

%% 3.
else 
    n_max = 150;

    % Analytical solution for f(x) = 1
    u_a1 = @(x) -(1/2) * (x - (1/2)).^2 + (1/8);
    % Analytical solution for f(x) = 2*delta(x-0.5)
    u_a2 = @(x) -abs(x - (1/2)) + (1/2);
    
    [err1, H1] = errorL2(n_max, @rhs_1, u_a1, x_span);
    [err2, H2] = errorL2(n_max, @rhs_2, u_a2, x_span);
    
    figure(1);
    loglog(H1, err1, 'DisplayName', 'f(x) = 1');
    hold on;
    loglog(H2, err2, '.', 'DisplayName', 'f(x) = 2*delta(x-0.5)');
    loglog(H1, H1.^2, 'DisplayName', 'O(h^2)');
    legend('Location', 'southoutside');
    title('Error over gridsize');
    xlabel('h');
    ylabel('Error in L^2 Norm');
    hold off;
end

%% Tidy up
clear choice; clear counter; clear i; clear j; clear si; clear n_i;