%% 1D Hat function
function [y] = hat_function(i, x, grid)
    grid_size = length(grid);
    x_size = length(x);
    n  = grid_size-2;
    h = (grid(end)-grid(1))/(n+1);
    % grid(i+1) ^= x_i
    y = zeros(size(x));
    for j = 1:x_size
        if ((x(j) > grid(i)) && (x(j) <= grid(i+1)))
            y(j) = (x(j) - grid(i))/h;
        elseif (x(j) > grid(i+1) && x(j) < grid(i+2))
            y(j) = (grid(i+2) - x(j))/h;
        end
    end
end