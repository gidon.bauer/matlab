%% Creates the stiffness matrix
function [A] = assembleMatrix(x_span, n)
    h = (x_span(2)-x_span(1))/(n+1);
    A = 2 * eye(n,n);
    for i = 1:(n-1)
        A(i+1,i) = -1;
        A(i,i+1) = -1;
    end
    A = sparse((1/h) .* A);
end