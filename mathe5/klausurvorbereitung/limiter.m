close all; clear; clc;

theta = linspace(-4, 4, 1000);

fig = figure(1);
fig.WindowState = 'maximized';
hold on;
plot(theta, superbee(theta), 'DisplayName', 'superbee');
plot(theta, minmod(theta), 'DisplayName', 'minmod');
plot(theta, vanLeer(theta), 'DisplayName', 'van Leer');
plot(theta, cslimiter(theta), 'DisplayName', 'Čada-Schmidtmann');
title('Limiter functions', 'FontSize', 20);
xlabel('\theta', 'FontSize', 20);
ylabel('\phi', 'FontSize', 20);
legend('Location', 'northwest', 'FontSize', 14);
hold off;

function phi = t3(theta)
    phi = (2 + theta) / 3;
end

function phi = cslimiter(theta)
    phi = zeros(size(theta));
    for i = 1:length(theta)
        phi(i) = max(0, min(t3(theta(i)), max(-theta(i), min(2*theta(i), min(t3(theta(i)), 1.5)))));
    end
end

function phi = superbee(theta)
    phi = zeros(size(theta));
    for i = 1:length(theta)
        phi(i) = max(0, max(min(1, 2*theta(i)), min(2, theta(i))));
    end
end

function phi = minmod(theta)
    phi = zeros(size(theta));
    for i = 1:length(theta)
        phi(i) = max(0, min(1, theta(i)));
    end
end

function phi = vanLeer(theta)
    phi = zeros(size(theta));
    for i = 1:length(theta)
        phi(i) = (theta(i) + abs(theta(i))) / (1 + theta(i));
    end
end