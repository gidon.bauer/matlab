%% Mathe V - Programieraufgabe 02
% Stefan Basermann, Adrien Bordes & Gidon Bauer

%% Clear the workspace
clear; clc; close all;

%% 
meshOps = MeshOperations('unitSquare1.msh');

meshOps.getMeshInformation();

% [A,b] = setEquationSystem(meshOps);