%% Non linear Euler equation
% u(1) ^= rho : density
% u(2) ^= rho*v : momentum
% u(3) ^= E : total energy
% pressure p = (gamma - 1) * (E - 0.5 * rho * v^2)

function [y] = f_euler(u)
    gamma = 1.4;
    
    p = (gamma - 1) * (u(3) - 0.5 * ((u(2)^2)/u(1)));

    y(1) = u(2);
    y(2) = (u(2)^2)/u(1) + p;
    y(3) = (u(2)/u(1)) * (u(3) + p);
end