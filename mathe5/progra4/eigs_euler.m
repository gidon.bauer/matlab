%% Calculates the eigenvalues for the given euler function
% u(1) ^= rho : density
% u(2) ^= rho*v : momentum
% u(3) ^= E : total energy
% pressure p = (gamma - 1) * (E - 0.5 * rho * v^2)
% speed of sound c = sqrt((gamma * p) / rho)

function [eigs] = eigs_euler(u)
    gamma = 1.4;
    p = (gamma - 1) * (u(3) - 0.5 * (u(2)^2/u(1)));
    c = sqrt((gamma * p) / u(1));

    eigs = (u(2) / u(1)) * ones(3,1);
    eigs(1) = eigs(1) - c;
    eigs(3) = eigs(3) + c;
end