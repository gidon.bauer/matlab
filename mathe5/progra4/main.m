%% Mathe V - Progammieraufgabe 04
% Stefan Basermann, Adrien Bordes & Gidon Bauer

%% Clear the workspace
clear; clc; close all;

%% Choose the part you want to test
choice = input('Choose which part you want to test. (1-2) ');

%% Define PDE
f = @f_euler;

%% Define Flux
flux = @HLL_flux;

%% Setup
N = 50;

xspan = [0;1];
tspan = [0;0.25];

courant_number = 0.8; % from (0,1]

%% Set initial conditions
% Note: this ul and ur contains [rho, v, p], while all other u further in
%       this programm will contain [rho, rho*v, E]
switch choice
    case 1
        ul = [1; 0; 1];
        ur = [0.125; 0; 0.1];
    case 2
        ul = [1; -1; 0.4];
        ur = [1; 1; 0.4];
    otherwise
        disp('Bad Input.');
        return;
end

u0 = init_cond(ul, ur, N);

%% Solving the PDE
[t, U] = FVM_3D(u0, flux, f, xspan, tspan, N, courant_number);


%% Plotting
x = linspace(xspan(1), xspan(2), N);

f = figure(1);
f.WindowState = 'maximized';
tiledlayout(2,2);

nexttile;
pcolor(t, x, U(:,:,1));
colorbar;
% surf(t, x, U(:,:,1));
title('Density');
xlabel('t');
ylabel('x');

nexttile;
pcolor(t, x, U(:,:,2));
colorbar;
% surf(t, x, U(:,:,2));
title('Momentum');
xlabel('t');
ylabel('x');

nexttile;
pcolor(t, x, U(:,:,3));
colorbar;
% surf(t, x, U(:,:,3));
title('Total energy');
xlabel('t');
ylabel('x');

%% Tidy up
clear choice;