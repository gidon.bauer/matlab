%% Set 3D inititial conditions with discontinuity at x = 0.5
% Note: this ul and ur contains [rho, v, p], while all other u further in
%       this programm will contain [rho, rho*v, E]
function [u0] = init_cond(ul, ur, N)
    u0 = zeros(N,3);

    gamma = 1.4;
    tot_energy = @(rho, v, p, gamma) (p/(gamma-1) + 0.5 * rho * v^2);

    rhol = ul(1);
    vl = ul(2);
    pl = ul(3);

    rhor = ur(1);
    vr = ur(2);
    pr = ur(3);

    El = tot_energy(rhol, vl, pl, gamma);
    Er = tot_energy(rhor, vr, pr, gamma);

    if (mod(N,2) == 0)
        u0(1:(N/2), 1) = rhol;
        u0((N/2)+1:end, 1) = rhor;

        u0(1:(N/2), 2) = rhol * vl;
        u0((N/2)+1:end, 2) = rhor * vr;

        u0(1:(N/2), 3) = El;
        u0((N/2)+1:end, 3) = Er;
    else
        u0(1:floor(N/2), 1) = rhol;
        u0(ceil(N/2), 1) = (rhor + rhol)/2;
        u0(ceil(N/2)+1:end, 1) = rhor;

        u0(1:floor(N/2), 2) = rhol * vl;
        u0(ceil(N/2), 2) = (rhor * vr + rhol * vl)/2;
        u0(ceil(N/2)+1:end, 2) = rhor * vr;

        u0(1:floor(N/2), 3) = El;
        u0(ceil(N/2), 3) = (Er + El)/2;
        u0(ceil(N/2)+1:end, 3) = Er;
    end
end