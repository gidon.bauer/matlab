%% HLL Flux function; works only for the euler function
function [F] = HLL_flux(func, ul, ur)
    eigs = zeros(6,1);
    eigs(1:3, 1) = eigs_euler(ul);
    eigs(4:6, 1) = eigs_euler(ur);

    al = min(eigs);
    ar = max(abs(eigs));

    if (0 <= al)
        F = func(ul);
    elseif ((al <= 0) && (0 <= ar))
        F = (ar * func(ul) - al * func(ur) + al * ar * (ur - ul)) / (ar - al);
    else
        F = func(ur);
    end
end