%% 3D Finite Volume method
function [t, U] = FVM_3D(u0, flux, f, xspan, tspan, N, courant_number)
    U = zeros(N, 1, 3); % Nx?x3
    
    dx = (xspan(2) - xspan(1)) / N;

    U(:, 1, :) = u0;
    
    t = [];
    t(1) = tspan(1);
    time = 1;
    while t(end) < tspan(2)
        dt = calc_dt(dx, U(:,time,:), courant_number);
        for space = 1:N
            if space == 1
%                 assuming periodic boundary conditions
%                 fluxl = flux(f, U(end, time, :), U(space, time, :));
%                 fluxr = flux(f, U(space, time, :), U(space+1, time, :));

%                 not assuming periodic boundary conditions
                fluxl = flux(f, U(space, time, :), U(space+1, time, :));
                fluxr = flux(f, U(space+1, time, :), U(space+2, time, :));
            elseif space == N
%                 assuming periodic boundary conditions
%                 fluxl = flux(f, U(space-1, time, :), U(space, time, :));
%                 fluxr = flux(f, U(space, time, :), U(1, time, :));

%                 not assuming periodic boundary conditions
                fluxl = flux(f, U(space-2, time, :), U(space-1, time, :));
                fluxr = flux(f, U(space-1, time, :), U(space, time, :));
            else
                fluxl = flux(f, U(space-1, time, :), U(space, time, :));
                fluxr = flux(f, U(space, time, :), U(space+1, time, :));
            end            
            U(space, time+1, 1) = U(space, time, 1) + (dt/dx) * (fluxl(1) - fluxr(1));
            U(space, time+1, 2) = U(space, time, 2) + (dt/dx) * (fluxl(2) - fluxr(2));
            U(space, time+1, 3) = U(space, time, 3) + (dt/dx) * (fluxl(3) - fluxr(3));
        end
        t(time+1) = t(time) + dt;
        time = time + 1;
    end
end