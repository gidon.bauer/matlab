%% Calculates the dt from dx and the eigenvalues of the jacobian
function [dt] = calc_dt(dx, U, courant_number)
    [si, ~, ~] = size(U);

    max_ev = 0;
    for ii = 1:si
        eigs = eigs_euler(U(ii, 1, :));
        max_ev = max(max_ev, max(abs(eigs)));
    end
    
    dt = (courant_number * dx)/max_ev;
end