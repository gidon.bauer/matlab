%% Mathe IV - Programieraufgabe 02
% Michel Mantsch, Jan Liebehenschel, Adrien Bordes & Gidon Bauer

%% Clear the workspace
clear; clc; close all;

%% Choose which part you want to test (1^= a,b; 2^= c)
choice = input('Choose which part you want to test. (1^= a,b; 2^= c) ');

%% Konvektions-Diffunsionsgleichung
if choice == 1
%% Setup
    n = 60;
    eps = 1e-4; % 1, 1e-2, 1e-4
    beta = (5*pi)/6;
    x = zeros(n+1,1);
    y = zeros(n+1,1);
    r = ones((n-1)^2,1); % Righthand side
    for i = 2:n+1
        x(i) = i/n;
        y(i) = i/n;
    end

%% Part a) - Upwind finite differnece
    A_2der = cfd2(n,eps);
    A_1der_ufd = ufd1(n,beta);
    A_ufd = A_2der + A_1der_ufd;
    u_udf  = A_ufd\r; % A_ufd * u_ufd = r
    U_ufd = zeros(n+1,n+1);
    for i = 1:n+1
        % Randbedingungen
        U_ufd(i,1) = 0;
        U_ufd(1,i) = 0;
        U_ufd(i,n+1) = 0;
        U_ufd(n+1,i) = 0;
    end
    for i = 2:n
        for j = 2:n
            U_ufd(j,i) = u_udf((i-2)*(n-1)+j-1);
        end
    end

    figure(1);
    surf(x,y,U_ufd);
    title('Using upwind finite differences');
    xlabel('x');
    ylabel('y');

%% Part b) - Central finite differnece
    A_2der = cfd2(n,eps);
    A_1der_cfd = cfd1(n,beta);
    A_cfd = A_2der + A_1der_cfd;
    u_cdf  = A_cfd\r; % A_cfd * u_cfd = r
    U_cfd = zeros(n+1,n+1);
    for i = 1:n+1
        % Randbedingungen
        U_cfd(i,1) = 0;
        U_cfd(1,i) = 0;
        U_cfd(i,n+1) = 0;
        U_cfd(n+1,i) = 0;
    end
    for i = 2:n
        for j = 2:n
            U_cfd(j,i) = u_cdf((i-2)*(n-1)+j-1);
        end
    end

    figure(2);
    surf(x,y,U_cfd);
    title('Using central finite differences');
    xlabel('x');
    ylabel('y');

else
%% Poisson-problem
%% Setup
    n = 31;
    x = zeros(n+1,1);
    y = zeros(n+1,1);

    for i = 2:n+1
        x(i) = i/n;
        y(i) = i/n;
    end
    
    U_poisson = zeros(n+1,n+1);
    for i = 1:n+1
        % Randbedingungen
        U_poisson(i,1) = x(i)^2;
        U_poisson(1,i) = y(i)^2;
        U_poisson(i,n+1) = x(i)^2 + 1;
        U_poisson(n+1,i) = 1 + y(i)^2;
    end
    
    r = zeros((n-1)^2,1); % Righthand side
    for i = 1:(n-1)^2
        r(i) = -4;
    end
    for i = 1:(n-1)
        r(i) = r(i) + (n^2)*U_poisson(i+1,1); % "oben"
        r((i-1)*(n-1)+1) = r((i-1)*(n-1)+1) + (n^2)*U_poisson(1,i+1); % "links"
        r((n-1)^2-(n-1)+i) = r((n-1)^2-(n-1)+i) + (n^2)*U_poisson(n+1,i+1); % "unten"
        r(i*(n-1)) = r(i*(n-1)) + (n^2)*U_poisson(i+1,n+1); % "rechts"
    end

%% Part c) - Only central finite difference order 2
    A_poisson = cfd2(n,1); % With eps=1 we get -Laplace(u)
    u_poisson  = A_poisson\r; % A_poisson * u_poisson = r
    for i = 2:n
        for j = 2:n
            U_poisson(j,i) = u_poisson((i-2)*(n-1)+j-1);
        end
    end

    figure(3);
    surf(x,y,U_poisson);
    title('Poisson Problem');
    xlabel('x');
    ylabel('y');

end

%% Tidy up
clear choice; clear i; clear j;

%% Central finite difference first order with beta
% cos(beta)*du/dx + sin(beta)*du/dy = r -> returns the matrix
function [A] = cfd1(n,beta)
    h = 1/n;
    
    % for du/dy
    T = zeros(n-1, n-1);
    for i = 1:(n-2)
        T(i+1,i) = 1;
        T(i,i+1) = -1;
    end
    T = sin(beta).*T;
    % for du/dx
    I = eye(n-1);
    I = cos(beta).*I;
    
    A = zeros((n-1)^2,(n-1)^2);
    for i = 1:(n-1):(n-1)^2
        A(i:i+(n-1)-1, i:i+(n-1)-1) = T; % diagonal
        if (i < (n-1)^2 - (n-1))
            A(i:i+(n-1)-1, i+(n-1):i+2*(n-1)-1) = I; % upper
            A(i+(n-1):i+2*(n-1)-1, i:i+(n-1)-1) = -I; % lower
        end
    end
    A = (1/(2*h)).*A;
end

%% Central finite difference second order with eps
% -eps*Laplace(u) = r -> returns the matrix
function [A] = cfd2(n,eps)
    h = 1/n;
    
    T = zeros(n-1,n-1);
    for i = 1:(n-1)
        T(i,i) = -4;
        if (i < n-1)
            T(i+1,i) = 1;
            T(i,i+1) = 1;
        end
    end
    I = eye(n-1);

    A = zeros((n-1)^2,(n-1)^2);
    for i = 1:(n-1):(n-1)^2
        A(i:i+(n-1)-1, i:i+(n-1)-1) = T;
        if (i < (n-1)^2 - (n-1))
            A(i:i+(n-1)-1, i+(n-1):i+2*(n-1)-1) = I;
            A(i+(n-1):i+2*(n-1)-1, i:i+(n-1)-1) = I;
        end
    end
    A = (-eps/h^2).*A;
end

%% Upwind finite difference first order with beta
% cos(beta)*du/dx + sin(beta)*du/dy = r -> returns the matrix
function [A] = ufd1(n,beta)
    h = 1/n;
    
    Ax = eye((n-1)^2,(n-1)^2);
    if (cos(beta) >= 0)
        Ix = -eye(n-1, n-1);
        for i = 1:(n-1):((n-1)^2-(n-1))
            Ax(i+(n-1):i+2*(n-1)-1, i:i+(n-1)-1) = Ix; % lower
        end        
    else
        Ax = -1.*Ax;
        Ix = eye(n-1, n-1);
        for i = 1:(n-1):((n-1)^2-(n-1))
            Ax(i:i+(n-1)-1, i+(n-1):i+2*(n-1)-1) = Ix; % upper
        end        
    end
    Ax = cos(beta).*Ax;
    
    Ay = zeros((n-1)^2,(n-1)^2);
    if (sin(beta) >= 0)
        Ty = zeros(n-1, n-1);
        for i = 1:(n-1)
            Ty(i,i) = 1;
            if (i < n-1)
                Ty(i,i+1) = -1; % lower
            end
        end
        for i = 1:(n-1):(n-1)^2
            Ay(i:i+(n-1)-1, i:i+(n-1)-1) = Ty;
        end
    else
        Ty = zeros(n-1, n-1);
        for i = 1:(n-1)
            Ty(i,i) = -1;
            if (i < n-1)
                Ty(i+1,i) = 1; % upper
            end
        end
        for i = 1:(n-1):(n-1)^2
            Ay(i:i+(n-1)-1, i:i+(n-1)-1) = Ty;
        end        
    end
    Ay = sin(beta).*Ay;
    
    A = (1/h).*(Ax + Ay);
end