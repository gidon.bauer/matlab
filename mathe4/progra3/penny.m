%% Mathe IV - Programieraufgabe 03
% Michel Mantsch, Jan Liebehenschel, Adrien Bordes & Gidon Bauer

%% Clear the workspace
clear; clc; close all;

%% Choose which part you want to test (1 ^= 1D Problem, 2 ^= Penny Problem)
choice = input('Choose which part you want to test. (1 ^= 1D Problem, 2 ^= Penny Problem) ');

if choice == 1
    %% Setup 1D Problem
    n = 30;
    tspan = [0; 0.1];
    x = (0:(1/n):1)';
    U0 = zeros(n-1, 1);
    for i = 1:(n-1)
        U0(i) = u01D(x(i+1));
    end

    %% Implcit Euler 1D
    UIE = zeros(n+1, n-1);
    [tIE, UIE(2:n, :)] = HeatImpEuler(U0, tspan);
        
    %% Crank Nicolson 1D
    UCN = zeros(n+1, n-1);
    [tCN, UCN(2:n, :)] = HeatCN(U0, tspan);
    
    %% Analytical 1D
    Ua = f1D(x, tIE);
    t = (tspan(1):(tspan(2) - tspan(1))/(n-2):tspan(2))';
    
    %% 1D Plots
    figure(1);
    tiledlayout(2,2);

    % Implicit Euler
    nexttile;
    surf(tIE, x, UIE);
    title('Implcit Euler Method');
    xlabel('time');
    ylabel('x');
    zlabel('u(x)');

    % Crank Nicolson
    nexttile;
    surf(tCN, x, UCN);
    title('Crank Nicolson Method');
    xlabel('time');
    ylabel('x');
    zlabel('u(x)');
    
    % Analytical
    nexttile;
    surf(t, x, Ua);
    title('Analytical Solution');
    xlabel('time');
    ylabel('x');
    zlabel('u(x)');

    % Max. error
    err_IE_str = "||Ua - UIE|| = " + num2str(norm(Ua-UIE, inf));
    err_CN_str = "||Ua - UCN|| = " + num2str(norm(Ua-UCN, inf));
    disp(err_IE_str);
    disp(err_CN_str);
    
%     nexttile;
%     axis off;
%     title('Max. error');
%     text(0.1, 0.5, err_IE_str);
%     text(0.1, 0.25, err_CN_str);

elseif choice == 2    
    %% Setup Penny
    load penny;
    P = flipud(P);
    n = 128/2; % The penny has size 128x128, we skip every 2nd point
    num_steps = n;
    tspan = [0; 1e-2];
    U0 = zeros(n^2, 1);
    for i = 1:n
        for j = 1:n
            U0(j + (i-1)*n) = P(2*j, 2*i);
        end
    end
    
    %% Implcit Euler Penny
    [tIE, uIE] = HeatImpEuler2D(U0, tspan, num_steps);
    
    % Create Matrix at t0
    UIE0 = zeros(n,n);
    for i = 1:n
        for j = 1:n
            UIE0(j,i) = uIE(j + (i-1)*n, 1);
        end
    end
    
    % Create Matrix at t_end
    UIEend = zeros(n,n);
    for i = 1:n
        for j = 1:n
            UIEend(j,i) = uIE(j + (i-1)*n, num_steps);
        end
    end
    
    figure(1);
    tiledlayout(2,2);
    % Plots at t0
    nexttile;
    contour(UIE0, 1:12:255);
    title("Implcit Euler Method at t = " + num2str(tspan(1)));
    colormap(copper);

    nexttile;
    surf(1:2:2*n, 1:2:2*n, UIE0);
    title("Implcit Euler Method at t = " + num2str(tspan(1)));
    colormap(copper);
    shading interp;

    % Plots at t_end
    nexttile;
    contour(UIEend, 1:12:255);
    title("Implcit Euler Method at t = " + num2str(tspan(2)));
    colormap(copper);

    nexttile;
    surf(1:2:2*n, 1:2:2*n, UIEend);
    title("Implcit Euler Method at t = " + num2str(tspan(2)));
    colormap(copper);
    shading interp;

    %% Crank Nicolson Penny
    [tCN, uCN] = HeatCN2D(U0, tspan, num_steps);
    
    % Create Matrix at t0
    UCN0 = zeros(n,n);
    for i = 1:n
        for j = 1:n
            UCN0(j,i) = uCN(j + (i-1)*n, 1);
        end
    end
    % Create Matrix at t_end
    UCNend = zeros(n,n);
    for i = 1:n
        for j = 1:n
            UCNend(j,i) = uCN(j + (i-1)*n, num_steps);
        end
    end
    
    figure(2);
    tiledlayout(2,2);
    
    % Plot at t0    
    nexttile;
    contour(UCN0, 1:12:255);
    title("Crank Nicolson Method at t = " + num2str(tspan(1)));
    colormap(copper);
    
    nexttile;
    surf(1:2:2*n, 1:2:2*n, UCN0);
    title("Crank Nicolson Method at t = " + num2str(tspan(1)));
    colormap(copper);
    shading interp;

    % Plot at t_end
    nexttile;
    contour(UCNend, 1:12:255);
    title("Crank Nicolson Method at t = " + num2str(tspan(2)));
    colormap(copper);

    nexttile;
    surf(1:2:2*n, 1:2:2*n, UCNend);
    title("Crank Nicolson Method at t = " + num2str(tspan(2)));
    colormap(copper);
    shading interp;
end

%% Tidy up
clear choice; clear i; clear j;

%% Start value side of 1D Problem
function [y] = u01D(x)
    y = sin(pi * x);
end

%% Analytical Solution of 1D Problem
function [y] = f1D(x, t)
    [n,~] = size(x);
    [m,~] = size(t);
    y = zeros(n,m);
    for i = 1:n
        for j = 1:m
            y(i,j) = sin(pi * x(i)) * exp(-pi^2 * t(j));
        end
    end
end

%% 1D Implicit Euler Method
% -> (d/dt)*u(t) = A*u(t)
function [t, U] = HeatImpEuler(U0, tspan)
    [n,~] = size(U0);

    U = zeros(n, n);
    U(:, 1) = U0;
    
    t = zeros(n, 1);
    t(1) = tspan(1);
    
    h = (tspan(2) - tspan(1))/(n-1);
    
    A = zeros(n, n);
    for i = 1:n
        A(i, i) = -2;
        if (i < n)
            A(i+1, i) = 1;
            A(i, i+1) = 1;
        end
    end
    
    A = n^2.*A;
    
    for i = 2:n
        t(i) = t(i-1) + h;
        % U(:, i) = U(:, i-1) + h*A*U(:, i);
        U(:, i) = (eye(n) - h*A)\U(:, i-1);
    end
end

%% 1D Crank Nicolson Method
% -> (d/dt)*u(t) = A*u(t)
function [t, U] = HeatCN(U0, tspan)
    [n, ~] = size(U0);

    U = zeros(n, n);
    U(:, 1) = U0;
    
    t = zeros(n, 1);
    t(1) = tspan(1);
    
    h = (tspan(2) - tspan(1))/(n-1);
    
    A = zeros(n, n);
    for i = 1:n
        A(i, i) = -2;
        if (i < n)
            A(i+1, i) = 1;
            A(i, i+1) = 1;
        end
    end
    
    A = n^2.*A;
    
    for i = 2:n
        t(i) = t(i-1) + h;
        % U(:, i) = U(:, i-1) + (h/2)*(A*U(:, i) + A*U(:, i-1));
        U(:, i) = (eye(n) - (h/2)*A)\((eye(n) + (h/2)*A)*U(:, i-1));
    end
end

%% 2D Implicit Euler Method
% -> (d/dt)u(t) = Au(t)
function [t, U] = HeatImpEuler2D(U0, tspan, steps)
    [m,~] = size(U0);
    n = sqrt(m);

    U = zeros(m, steps);
    U(:, 1) = U0;
    
    t = zeros(steps, 1);
    t(1) = tspan(1);
    
    h = (tspan(2) - tspan(1))/(steps-1);
    
    T = zeros(n, n);
    for i = 1:n
        T(i, i) = -2;
        if (i < n)
            T(i+1, i) = 1;
            T(i, i+1) = 1;
        end
    end
    
    I = eye(n);

    A = kron(T,I) + kron(I,T);
    A = n^2.*A;
    
    for i = 2:steps
        t(i) = t(i-1) + h;
        % U(:, i) = U(:, i-1) + h*A*U(:, i);
        U(:, i) = (eye(m) - h*A)\U(:, i-1);
    end
end

%% 2D Crank Nicolson Method
% -> (d/dt)u(t) = Au(t)+b -> RB
function [t, U] = HeatCN2D(U0, tspan, steps)
    [m,~] = size(U0);
    n = sqrt(m);

    U = zeros(m, steps);
    U(:, 1) = U0;
    
    t = zeros(steps, 1);
    t(1) = tspan(1);
    
    h = (tspan(2) - tspan(1))/(steps-1);
    
    T = zeros(n, n);
    for i = 1:n
        T(i, i) = -2;
        if (i < n)
            T(i+1, i) = 1;
            T(i, i+1) = 1;
        end
    end
    
    I = eye(n);

    A = kron(T,I) + kron(I,T);
    A = n^2.*A;
    
    % RB
    b = zeros(n^2, 1);
    for i = 1:n
        b(i) = (n^2) * U0(i); % "oben"
        b(1 + n*(i-1)) = (n^2) * U0(1 + n*(i-1)); % "links"
        b(i + n^2-n) = (n^2) * U0(i + n^2-n); % "unten"
        b(n + n*(i-1)) = (n^2) * U0(n + n*(i-1)); % "rechts"
    end

    for i = 2:steps
        t(i) = t(i-1) + h;
        % U(:, i) = U(:, i-1) + (h/2)*(A*U(:, i) + A*U(:, i-1)) + h*b;
%         U(:, i) = (eye(m) - (h/2)*A)\(((eye(m) + (h/2)*A)*U(:, i-1) - h*b);
        U(:, i) = (eye(m) - (h/2)*A)\(((eye(m) + (h/2)*A)*U(:, i-1)) + h*b);
    end
end