%% Mathe IV - Programmieraufgabe 01
%% Adrien Bordes, Gidon Bauer, Jan Liebehenschel & Michel Mantsch

%% Clear the workspace
clear; clc; close all;

%% Choose which part you want to test
choice = 3;

%% Part 1a)
if choice == 1
    N = 4;
    M = 5;
    x = zeros(N, 1);
    y = zeros(M, 1);
    for i = 0:(N-1)
       x(i+1,1) = ((2*i+1)*pi) / (2*N);
    end
    for i = 0:(M-1)
        y(i+1,1) = ((2*i+1)*pi) / (2*M);
    end

    F = zeros(N, M);
    for i = 1:N
        for j = 1:M
            F(i, j) = cos(2*x(i,1)) + cos(3*y(j,1));
        end
    end

    D = DCT(F, x, y);
    disp(D);

%% Part 1b)
elseif choice == 2
    n_max = 50;
    P = (2:n_max)';
    err_array = zeros((n_max-1), 1);
    for i = 1:(n_max-1)
        err_array(i,1) = max_error(P(i));
    end
    figure(1);
    plot(P, err_array);
    title('Error over number of steps');
    xlabel('Number of steps');
    ylabel('Max. Error');

%% Part 2
elseif choice == 3
    RGB = imread('peppers.png');
    F = 255 .* im2double(rgb2gray(RGB));
    figure(1);
    imshow(F, 'DisplayRange', []);
    title('Original Image');
    
    A = JPEG(F);
    figure(2);
    imshow(A, 'DisplayRange', []);
    title('Compressed Image');
end

%% Tidy up
clear choice; clear i; clear j;

%% Part 1b) as function
function [max_err] = max_error(P)
    x = zeros((P-1), 1);
    y = zeros((P-1), 1);
    h = pi/P;
    for i = 1:(P-1)
        x(i,1) = i*h;
        y(i,1) = i*h;
    end
   

    F = zeros((P-1), (P-1));
    for i = 1:(P-1)
        for j = 1:(P-1)
            F(i, j) = (x(i,1) - pi/2)^2 + (y(j,1) - pi/2)^2;
        end
    end
   
    D = DCT(F, x, y);
    A = TDCT(D, x, y);
   
    max_err = max(max(abs(A-F)));    
end

%% c-function
function [y] = c(x)
    if x == 0
        y = 1/2;
    else 
        y = 1;
    end
end

%% Calculate the coefficients
function [D] = DCT(F, x, y)
    [sx, sy] = size(F);
    D = zeros(sx, sy);
    for i = 1:sx
        for j = 1:sy
            for k = 1:sx
                for l = 1:sy
                    D(i,j) = D(i,j) + F(k,l)*cos((i-1)*x(k,1))*cos((j-1)*y(l,1)) * (4/(sx*sy));
                end
            end
        end
    end
end

%% Evaluate the trigonometric polynomial
function [A] = TDCT(D,x,y)
    [sx, sy] = size(D);
    A = zeros(sx, sy);
    for i = 1:sx
        for j = 1:sy
            for k = 1:sx
                for l = 1:sy
                    A(i,j) = A(i,j) + D(k,l)*c(k-1)*c(l-1)*cos((k-1)*x(i,1))*cos((l-1)*y(j,1));
                end
            end
        end
    end
end

%% 
function [A] = JPEG(F)
    [sx, sy] = size(F);
    s_temp = 8; % We cut out matrizes of the size 8x8
    % Used to compress our image
    sigma = [10 15 25 37 51 66 82 100;
             15 19 28 39 52 67 83 101;
             25 28 35 45 58 72 88 105;
             37 39 45 54 66 79 94 111;
             51 52 58 66 76 89 103 119;
             66 67 72 79 89 101 114 130;
             82 83 88 94 103 114 127 142;
             100 101 105 111 119 130 142 156];

    A = zeros(sx, sy);

    for k = 1:s_temp:sx
        for l = 1:s_temp:sy
            F_temp = F(k:(k+s_temp-1), l:(l+s_temp-1));
            x = zeros(s_temp, 1);
            y = zeros(s_temp, 1);
            for i = 0:(s_temp-1)
                x(i+1,1) = ((2*i+1)*pi) / (2*s_temp);
                y(i+1,1) = ((2*i+1)*pi) / (2*s_temp);
            end
            D_temp = DCT(F_temp, x, y);
            D_temp = round(D_temp / sigma);
            A_temp = TDCT(D_temp, x, y);
            
            A(k:(k+s_temp-1), l:(l+s_temp-1)) = A_temp;
        end
    end
end