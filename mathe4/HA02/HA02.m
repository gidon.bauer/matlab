clear; clc; close all;

min = -5;
max = 5;

x = min:0.1:max;
y = min:0.1:max;

contourf(x, y, q(x,y)'); % Wenn nicht transponiert sind die Achsen vertauscht(?)
xlabel('x');
ylabel('y');
title('Höhenlinien von q(x,y)');

function [z] = q(x, y)
    A = [2,3;
         3,10];

    [~, sx] = size(x);
    [~, sy] = size(y);
    
    z = zeros(sx, sy);
     
    for i = 1:sx
        for j = 1:sy
            v = [x(i); y(j)];
            z(i,j) = v' * A * v;
        end
    end
end