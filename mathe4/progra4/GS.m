%% Gauss-Seidel Method
function [u,steps] = GS(A,b)
    eps = 1e-4;
    [n,~] = size(A);
    u = zeros(n,1);
    steps = 0;
    while (norm(b - A*u, inf) >= eps)
        u = u + tril(A)\(b - A*u);
        steps = steps + 1;
    end
end