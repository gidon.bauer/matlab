%% Mathe IV - Programieraufgabe 04
% Michel Mantsch, Jan Liebehenschel, Adrien Bordes & Gidon Bauer

%% Clear the workspace
clear; clc; close all;

%% Choose which part you want to test (1-2)
choice = input('Choose which part you want to test. (1-2) ');

%% a)
if choice == 1
%% Setup a)
    n = 50;

    x = (0:(1/(n+1)):1)';
    y = (0:(1/(n+1)):1)';
    
    A = cfd2(n, 1);
    b = zeros(n^2, 1); % Righthand side
    b(:,1) = -4;
    for i = 1:n
        b(i)           = b(i)           + ((n+2)^2) * y(i+1)^2;       % "oben"
        b(1 + n*(i-1)) = b(1 + n*(i-1)) + ((n+2)^2) * x(i+1)^2;       % "links"
        b(i + n^2-n)   = b(i + n^2-n)   + ((n+2)^2) * (1 + y(i+1)^2); % "unten"
        b(n + n*(i-1)) = b(n + n*(i-1)) + ((n+2)^2) * (x(i+1)^2 + 1); % "rechts"
    end

    %% Calculations a)
    [u_JC, ~] = JC(A,b);
    U_JC = transform(u_JC, x, y);

    [u_GS, ~] = GS(A,b);
    U_GS = transform(u_GS, x, y);
    
    u_a = A\b;
    U_a = transform(u_a, x, y);
    
    %% Plots a)
    figure(1);
    tiledlayout('flow');

    nexttile;
    surf(x, y, U_JC);
    title('Jacobi Method');
    xlabel('x');
    ylabel('y');
    zlabel('u(x,y)');

    nexttile;
    surf(x, y, U_GS);
    title('Gauss-Seidel Method');
    xlabel('x');
    ylabel('y');
    zlabel('u(x,y)');

    nexttile;
    surf(x, y, U_a);
    title('MATLAB function');
    xlabel('x');
    ylabel('y');
    zlabel('u(x,y)');

%% b)
elseif choice == 2
%% Setup b)
    start_n = 10; width = 10; end_n = 100;
    N = (start_n:width:end_n)';
    [num_points,~] = size(N);
    time_JC = zeros(num_points, 1);
    all_steps_JC = zeros(num_points, 1);
    avg_time_JC = zeros(num_points, 1);

    time_GS = zeros(num_points, 1);
    all_steps_GS = zeros(num_points, 1);
    avg_time_GS = zeros(num_points, 1);
    
    pos = 1;
    for n = start_n:width:end_n
        x = (0:(1/(n+1)):1)';
        y = (0:(1/(n+1)):1)';

        A = cfd2(n,1);
        b = zeros(n^2,1); % Righthand side
        b(:,1) = -4;
        for i = 1:n
            b(i)           = b(i)           + ((n+2)^2) * y(i+1)^2;       % "oben"
            b(1 + n*(i-1)) = b(1 + n*(i-1)) + ((n+2)^2) * x(i+1)^2;       % "links"
            b(i + n^2-n)   = b(i + n^2-n)   + ((n+2)^2) * (1 + y(i+1)^2); % "unten"
            b(n + n*(i-1)) = b(n + n*(i-1)) + ((n+2)^2) * (x(i+1)^2 + 1); % "rechts"
        end

        %% Calculations b)
        time0_JC = tic;
        [~, all_steps_JC(pos)] = JC(A,b);
        time_JC(pos) = toc(time0_JC);
        avg_time_JC(pos) = time_JC(pos) / all_steps_JC(pos);

        time0_GS = tic;
        [~, all_steps_GS(pos)] = GS(A,b);
        time_GS(pos) = toc(time0_GS);        
        avg_time_GS(pos) = time_GS(pos) / all_steps_GS(pos);
        
        pos = pos + 1;
    end
    
    %% Plots b)
    figure(1);
    tiledlayout('flow');
    % time over n^2
    nexttile;
    loglog(N.^2, time_JC, 'DisplayName', 'Jacobi');
    xlabel('n^2');
    ylabel('time [s]');
    title('time over n^2');
    hold on;
    loglog(N.^2, time_GS, 'DisplayName', 'Gauss-Seidel');
    legend('Location', 'northwest');
    hold off;
    
    % steps over n^2
    nexttile;
    loglog(N.^2, all_steps_JC, 'DisplayName', 'Jacobi');
    xlabel('n^2');
    ylabel('steps');
    title('number of steps over n^2');
    hold on;
    loglog(N.^2, all_steps_GS, 'DisplayName', 'Gauss-Seidel');
    loglog(N.^2, N.^2, 'DisplayName', 'O(n^2)');
    legend('Location', 'northwest');
    hold off;
    
    % avg time over n^2
    nexttile;
    loglog(N.^2, avg_time_JC, 'DisplayName', 'Jacobi');
    xlabel('n^2');
    ylabel('avg. time [s]');
    title('avg. time over n^2');
    hold on;
    loglog(N.^2, avg_time_GS, 'DisplayName', 'Gauss-Seidel');
    legend('Location', 'northwest');
    hold off;

end

%% Tidy up
clear choice; clear i; clear start_n; clear num_points; clear end_n; clear width; clear pos;
clear time0_JC; clear time0_GS;