%% Central finite difference second order with eps
% -eps*Laplace(u) = r -> returns the matrix
function [A] = cfd2(n,eps)
    h = 1/(n+2);
    T = -2 * eye(n);
    for i = 1:(n-1)
        T(i+1,i) = 1;
        T(i,i+1) = 1;
    end
    I = eye(n);
    A = kron(T,I) + kron(I,T);
    A = sparse((-eps/h^2).*A);
end