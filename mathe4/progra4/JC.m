%% Jacobi Method
function [u,steps] = JC(A,b)
    eps = 1e-4;
    [n,~] = size(A);
    C = sparse(diag(diag(A).^(-1)));
    u = zeros(n,1);
    steps = 0;
    while (norm(b - A*u, inf) >= eps)
        u = u + C*(b - A*u);
        steps = steps + 1;
    end
end