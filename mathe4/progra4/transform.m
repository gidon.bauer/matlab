%% Transform vector into matrix with border conditions
function [U] = transform(u, x, y)
    [n,~] = size(u); n = sqrt(n);
    U = zeros(n+2, n+2);
    for i = 1:n+2
        % Randbedingungen -> x^2 + y^2
        U(i,1) = x(i)^2;
        U(1,i) = y(i)^2;
        U(i,n+2) = x(i)^2 + 1;
        U(n+2,i) = 1 + y(i)^2;
    end
    for i = 1:n
        for j = 1:n
            U(j+1,i+1) = u(j + n*(i-1));
        end
    end
end