%% Cleanup
clc;
clear;
close all;

%% Setup
nX = 500;
nY = 500;

xspan = [-5,5];
yspan = [-5,5];

dx = (xspan(2) - xspan(1)) / (nX - 1);
dy = (yspan(2) - yspan(1)) / (nY - 1);

%% Create Grid
x = zeros(nX,nY);
y = zeros(nX,nY);

for i = 1:nX
    for j = 1:nY
        x(i,j) = xspan(1) + (i - 1) * dx;
        y(i,j) = yspan(1) + (j - 1) * dy;
    end
end

%% Calculate
% Potential
phi = pot_func(x,y);
% Stromfunktion
psi = str_func(x,y);
% Geschwindigkeit
[u,v] = vel(x,y);

% figure(1);
% contour(x, y, phi);
figure(2);
contour(x, y, psi);
% figure(3);
% pcolor(x,y,u);
% figure(4);
% pcolor(x,y,v);

%% Potentialfunktion
% Halbkörper => Parallelströmung + Quelle
function phi = pot_func(x,y)
    [nX,nY] = size(x);
    phi = zeros(nX,nY);
    
    % Definitionen
    uInf = 5;
    vInf = 0;
    E = 10;
    
    for i = 1:nX
        for j = 1:nY
            phi(i,j) = uInf * x(i,j) + vInf * y(i,j) + ...
                       (E/(2*pi)) * log(sqrt(x(i,j)^2 + y(i,j)^2));
        end
    end
    
end

%% Stromfunktion
% Halbkörper => Parallelströmung + Quelle
function psi = str_func(x,y)
    [nX,nY] = size(x);
    psi = zeros(nX,nY);
    
    % Definitionen
    uInf = 5;
    vInf = 0;
    E = 10;
    
    for i = 1:nX
        for j = 1:nY
            atan_val = 0;
            if (x(i,j) > 0)
                atan_val = atan(y(i,j)/x(i,j));
            elseif (x(i,j) < 0)
                if (y(i,j) > 0)
                    atan_val = atan(y(i,j)/x(i,j)) + pi;
                else
                    atan_val = atan(y(i,j)/x(i,j)) - pi;
                end
            else
                if (y(i,j) > 0)
                    atan_val = pi/2;
                else
                    atan_val = -pi/2;
                end
            end
                
                    
                    
            psi(i,j) = uInf * y(i,j) - vInf * x(i,j) + ...
                       (E/(2*pi)) * atan_val;
        end
    end
    
end

%% Geschwindigkeiten
% Halbkörper => Parallelströmung + Quelle
function [u,v] = vel(x,y)
    [nX,nY] = size(x);
    u = zeros(nX,nY);
    v = zeros(nX,nY);
    
    % Definitionen
    uInf = 5;
    vInf = 0;
    E = 10;
    
    for i = 1:nX
        for j = 1:nY
            u(i,j) = uInf + (E/(2*pi)) * x(i,j)/(x(i,j)^2 + y(i,j)^2);
            v(i,j) = vInf + (E/(2*pi)) * y(i,j)/(x(i,j)^2 + y(i,j)^2);
        end
    end
end
