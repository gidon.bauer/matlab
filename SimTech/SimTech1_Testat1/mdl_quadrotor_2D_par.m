function Par = mdl_quadrotor_2D_par( )
% Par = mdl_quadrotor_2D_par( )
%
%   Author1     : 
%
%   Last change : Spring 2019
%
%   Description : Sets the parameters of a 2D quadrotor model
% 
%   Parameters  : None
% 
%   Return      : Par -> Struct containing the model parameters
% 
%-------------------------------------------------------------------------%

