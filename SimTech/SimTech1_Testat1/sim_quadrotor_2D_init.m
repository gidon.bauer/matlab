clear all; close all; clc;

% Load quadrotor model parameters
ParQuad2D = mdl_quadrotor_2D_par();

% Load input trajectory
load('data_inputTrajectory.mat')