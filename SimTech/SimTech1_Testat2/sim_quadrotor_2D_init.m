clear all; close all; clc;

% Load quadrotor model parameters
ParQuad2D = mdl_quadrotor_2D_par();
ParPD     = mdl_control_pd_par(ParQuad2D);