function Par = mdl_control_pd_par( ParQuad )
% Par = mdl_control_pd_par( ParQuad )
%
%   Author1     : 
%
%   Last change : Spring 2019
%
%   Description : Sets the parameters of the 2D quadrotor tilt control
% 
%   Parameters  : ParQuad -> Struct containing model parameters
% 
%   Return      : Par -> Struct containing the PD control parameters
% 
%-------------------------------------------------------------------------%
Par.Td = 0.4;
Par.K  = 0.0058;
Par.F0 = 0.5 * 0.55 * 9.81;
Par.armLength = ParQuad.armLength;