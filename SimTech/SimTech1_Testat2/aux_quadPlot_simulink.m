classdef aux_quadPlot_simulink < matlab.System & handle & matlab.system.mixin.Propagates
% Visualization class for quadrotor simulation
%
%   Author1     : Pennsylvania State University
%   Author2     : Eugen Nuss (e.nuss@irt.rwth-aachen.de)
% 
%   Last change :
%
%   Description : Visualization class for quadrotor simulation
%
%   Attributes  : 
% 
%   Methods     : 
% 
%-------------------------------------------------------------------------%




    %---------------------------------------------------------------------%
    % Set attributes                                                      %
    %---------------------------------------------------------------------%
    properties (Access = public)
        qn       = 1;               % Quad number
        color    = [0 0.447 0.741]; % Color of quad
        wingspan = 0.1;             % Wingspan
        cstep    = 0.1;             % Image capture time interval/ Update step width [s]
        max_time = 50;
        height   = 0.04;            % Height of quad
        text_dist = 0.03;           % Distance of quad number to quad
        drawspeed = 0.02;   
    end
    

    properties(DiscreteState)
        state_hist     % Position history
        state_des_hist % Desired position history
        time_hist      % Time history
        k              % Counter
        k_int          % Counter if xk+1 = xk+T for T = 0 or T 
    end
    

    % Pre-computed constants
    properties(Access = private)
        h_3d           % Axis-handle of 3D plot
        h_xz           % Axis-handle of x-z-plane
        h_yz           % Axis-handle of y-z-plane
        h_xy           % Axis-handle of x-y-plane
        h_xzBody       % Axis-handle of body fixed x-z-plane
        h_yzBody       % Axis-handle of body fixed y-z-plane
  
        motor;          % Motor position
    end
    
    %---------------------------------------------------------------------%
    
    
    
    
    %---------------------------------------------------------------------%
    % Define methods                                                      %
    %---------------------------------------------------------------------%
    methods(Access = protected)
        function [sz,dt,cp] = getDiscreteStateSpecificationImpl(obj,name)
            switch name
                case {'state_hist', 'state_des_hist', 'time_hist'}
                    nCol = obj.max_time/obj.cstep;
                    sz   = [6 nCol];
                    dt = 'double';
                    cp = false;
                case {'k', 'k_int'}
                    sz = [1 1];
                    dt = 'double';
                    cp = false;
                otherwise
                    error(['Error: Incorrect State Name: ', name]);
            end
        end
        
        function dt1 = getOutputDataTypeImpl(~)
        	dt1 = 'double';
        end
        function cp1 = isOutputComplexImpl(~)
        	cp1 = false;
        end
        function fz1 = isOutputFixedSizeImpl(~)
        	fz1 = true;
        end
        function num = getNumOutputsImpl(~)
            num = 1;
        end
        function sze = getOutputSizeImpl(~)
            sze = [1 1];
        end
        
        function setupImpl(obj, ~, state, stateDesired, ~)
            % Perform one-time calculations, such as computing constants
            close all;
            disp('Initializing figures...');
            
            figure;
            sp3d = subplot(3,3,[1,2,4,5]);
            obj.h_3d = hggroup;
            axis equal
            grid on
            view(3);
            xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]')
            
            spxz = subplot(333);
            obj.h_xz = hggroup;
            axis equal
            grid on
            box on
            view(0,0);
            xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]')
            
            spyz = subplot(336);
            obj.h_yz = hggroup;
            axis equal
            grid on
            box on
            view(90,0);
            xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]')
            
            spxy = subplot(339);
            obj.h_xy = hggroup;
            axis equal
            grid on
            box on
            view(0,90);
            xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]')
            
            spxzBody = subplot(337);
            obj.h_xzBody = hggroup;
            axis equal
            grid on
            box on
            view(0,0);
            xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]')
            
            spyzBody = subplot(338);
            obj.h_yzBody = hggroup;
            axis equal
            grid on
            box on
            view(90,0);
            xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]')
            
            UpdateQuadState(obj, state)
            
            % Initialize 3D plot handle            
            groupHandles = {obj.h_xzBody, obj.h_yzBody, obj.h_3d,  ...
                            obj.h_xz, obj.h_yz, obj.h_xy};
            subPlots = {spxzBody, spyzBody, sp3d, spxz, spyz, spxy};
            for ii = 1:length(groupHandles)
                hold(subPlots{ii}, 'on')
                if ii>2
                    plot3(state(1), state(2), state(3), 'r.', 'parent', groupHandles{ii});
                    plot3(stateDesired(1), stateDesired(2), stateDesired(3), 'b.', 'parent', groupHandles{ii});
                end
                plot3(obj.motor(1,[1 3]), ...
                              obj.motor(2,[1 3]), ...
                              obj.motor(3,[1 3]), ...
                              '-ko', 'MarkerFaceColor', obj.color, 'MarkerSize', 5, 'parent', groupHandles{ii});
                plot3(...
                    obj.motor(1,[2 4]), ...
                    obj.motor(2,[2 4]), ...
                    obj.motor(3,[2 4]), ...
                    '-ko', 'MarkerFaceColor', obj.color, 'MarkerSize', 5, 'parent', groupHandles{ii});
                plot3(...
                    obj.motor(1,[5 6]), ...
                    obj.motor(2,[5 6]), ...
                    obj.motor(3,[5 6]), ...
                    'Color', obj.color, 'LineWidth', 2, 'parent', groupHandles{ii});
                text(...
                    obj.motor(1,5) + obj.text_dist, ...
                    obj.motor(2,5) + obj.text_dist, ...
                    obj.motor(3,5) + obj.text_dist, num2str(obj.qn), 'parent', groupHandles{ii});
                hold(subPlots{ii}, 'off')
            end
            disp('Done');
        end
        
        
        function UpdateQuadState(obj, state)
            % Update motor position
            % quadpos function
            % QUAD_POS Calculates coordinates of quadrotor's position in world frame
            % pos   3x1 position vector [x; y; z];
            % rot   3x3 body-to-world rotation matrix
            % L     1x1 length of the quad
            % wRb = RPYtoRot_ZXY(euler(1), euler(2), euler(3))';            
            % Update quad state
            [~,rot] = aux_rotEuler( zeros(3,1), state(7:9), 'to-inertial-frame');
            
            % quadpos function
            wHb            = [rot state(1:3); 0 0 0 1]; % homogeneous transformation from body to world
            quadBodyFrame  = [obj.wingspan 0 0 1; 0 obj.wingspan 0 1; ...
                             -obj.wingspan 0 0 1; 0 -obj.wingspan 0 1; ...
                             0 0 0 1; 0 0 obj.height 1]';
            quadWorldFrame = wHb * quadBodyFrame;
            obj.motor      = quadWorldFrame(1:3, :);
        end

                
        function UpdateQuadHist(obj, time, state, des_state)
            % Update quad history
            obj.k = obj.k + 1;
            obj.time_hist(obj.k) = time;
            obj.state_hist(:,obj.k) = state(1:6);
            obj.state_des_hist(:,obj.k) = des_state(1:6);
        end
        
        

        function y = stepImpl(obj, b3dPlot, state, stateDesired, time)
            % Implement algorithm. Calculate y as a function of input u and
            % discrete states.
            iter = mod(time, obj.cstep);

            if b3dPlot && iter==0 && obj.k_int==0
                obj.k_int = 1;
                obj.UpdateQuadState(state);
                obj.UpdateQuadHist(time, state, stateDesired);

                groupHandles = {obj.h_xzBody, obj.h_yzBody, obj.h_3d,...
                                obj.h_xz, obj.h_yz, obj.h_xy};
                for ii = 1:length(groupHandles)
                    tmpLine = get(groupHandles{ii}, 'Children');
                    if ii<=2
                        set(tmpLine(4), ...
                            'XData', obj.motor(1,[2 4]), ...
                            'YData', obj.motor(2,[2 4]), ...
                            'ZData', obj.motor(3,[2 4]));
                        set(tmpLine(2), ...
                            'XData', obj.motor(1,[5 6]), ...
                            'YData', obj.motor(2,[5 6]), ...
                            'ZData', obj.motor(3,[5 6]))
                        set(tmpLine(3), ...
                            'XData', obj.motor(1,[1 3]), ...
                            'YData', obj.motor(2,[1 3]), ...
                            'ZData', obj.motor(3,[1 3]));
                        set(tmpLine(1),  'Position', ...
                           [obj.motor(1,5) + obj.text_dist, ...
                            obj.motor(2,5) + obj.text_dist, ...
                            obj.motor(3,5) + obj.text_dist]);
                    else
                        set(tmpLine(5), ...
                            'XData', obj.state_des_hist(1,1:obj.k), ...
                            'YData', obj.state_des_hist(2,1:obj.k), ...
                            'ZData', obj.state_des_hist(3,1:obj.k));
                        set(tmpLine(6), ...
                            'XData', obj.state_hist(1,1:obj.k), ...
                            'YData', obj.state_hist(2,1:obj.k), ...
                            'ZData', obj.state_hist(3,1:obj.k));
                        set(tmpLine(3), ...
                            'XData', obj.motor(1,[2 4]), ...
                            'YData', obj.motor(2,[2 4]), ...
                            'ZData', obj.motor(3,[2 4]));
                        set(tmpLine(2), ...
                            'XData', obj.motor(1,[5 6]), ...
                            'YData', obj.motor(2,[5 6]), ...
                            'ZData', obj.motor(3,[5 6]))
                        set(tmpLine(4), ...
                            'XData', obj.motor(1,[1 3]), ...
                            'YData', obj.motor(2,[1 3]), ...
                            'ZData', obj.motor(3,[1 3]));
                        set(tmpLine(1),  'Position', ...
                           [obj.motor(1,5) + obj.text_dist, ...
                            obj.motor(2,5) + obj.text_dist, ...
                            obj.motor(3,5) + obj.text_dist]);
                    
                    end
                end
                pause(obj.drawspeed)
                y = 1;
            elseif b3dPlot && iter==0 && obj.k_int==1
                obj.k_int = 0;
                y = 0;
            else
                y = 0;
            end
        end

        
        function resetImpl(obj)
            % Initialize / reset discrete-state properties
            nCol = obj.max_time/obj.cstep;
            obj.time_hist = zeros(6, nCol);
            obj.state_hist = zeros(6, nCol);
            obj.state_des_hist = zeros(6, nCol);
            obj.k = 0;
            obj.k_int = 0;
        end
    end
    
    %---------------------------------------------------------------------%
    
    
end
