function Par = mdl_quadrotor_2D_par( )
% Par = mdl_quadrotor_2D_par( )
%
%   Author1     : Eugen Nuss (e.nuss@irt.rwth-aachen.de)
%
%   Last change : Spring 2019
%
%   Description : Sets the parameters of a 2D quadrotor model
% 
%   Parameters  : None
% 
%   Return      : Par -> Struct containing the model parameters
% 
%-------------------------------------------------------------------------%

Par.mass      = 0.55;       % Quadrocopter mass [kg]
Par.gravity   = 9.81;       % Earth acceleration [m*s^-2]
Par.armLength = 0.086;      % Distance to rotor from center point [m]
Par.theta     = 2.320e-04;  % Inertia matrix

Par.F0        = Par.mass*Par.gravity/2;