% nmot = linspace(1500, 4500);

% plot(nmot, velocity(nmot ./ 60));

disp('Geschwindigkeit');
for n = 1500:500:4000
    disp(velocity(n/60));
end

disp('Motormoment');
for p = 1:10
    disp(moment(p * 1e5));
end

disp('Zugkraft');
for p = 1:10
    disp(force(p * 1e5));
end

disp('Bedarfsleistung');
for n = 1500:500:4000
    disp(needed_force(n/60) * velocity(n/60) * 1e-3);
end

function Fbed = needed_force(nmot)
    fr = 0.011;
    m = 1495;
    g = 9.81;
    cw = 0.245;
    rho = 1.2;
    A = 2.01;
    
    Fbed = fr * m * g + 0.5 * cw * rho * A * velocity(nmot);
end

function Fz = force(pme)
    iv = 2.3;
    i4  = 1;
    rdyn = 0.301;
    ny4 = 0.98;
    Mverl = 3.354;
    
    Fz = ((moment(pme) - Mverl) * iv * i4 * ny4) / rdyn;
end

function Mmot = moment(pme)
    VH = 2499e-6;
    i = 0.5;
    
    Mmot = (VH * i * pme) / (2 * pi);
end

function vfzg = velocity(nmot)
    iv = 2.3;
    i4 = 1;
    rdyn = 0.301;
    
    
    vfzg = (2 * pi * nmot * rdyn) / (iv * i4);
end