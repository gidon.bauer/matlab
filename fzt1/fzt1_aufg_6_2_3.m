%% Clean up
clc; clear; close all;
%%
vf = linspace(0, 40);
figure(1);
hold on;
plot(vf, P_bed(vf), 'DisplayName', 'P_B_e_d');
xlabel('v_F [m/s]');
ylabel('Leistung [kW]');
hold off;

% function Pnetto = P_netto(vf)
%     
% end

function Pbed = P_bed(vf)
    fr = 0.02;
    m = 1000;
    g = 9.81;
    cw = 0.4;
    rhol = 1.2;
    A = 1.8;
    
    Pbed = fr*m*g*vf + 0.5*cw*rhol*A*vf.^3;
    Pbed = Pbed .* 1e-3;
end