%% Clean up
clear; clc; close all;

%%
FBv_over_Fz = linspace(0, 0.6, 10000);

ideal_FBh_over_Fz = ideal_breaking_dist(FBv_over_Fz);
real_FBh_over_Fz = real_breaking_dist(FBv_over_Fz);

diff = abs(ideal_FBh_over_Fz - real_FBh_over_Fz);

intersectsx = [];
intersectsy = [];
j = 1;
for i = 1:length(diff)
    if (diff(i) <= 1e-5)
        intersectsx(j) = FBv_over_Fz(i);
        intersectsy(j) = real_FBh_over_Fz(i);
        j = j+1;
    end
end

figure(1);
hold on;
plot(FBv_over_Fz, ideal_FBh_over_Fz, 'DisplayName', 'ideal');
plot(FBv_over_Fz, real_FBh_over_Fz, 'DisplayName', 'real');

plot(intersectsx, intersectsy, 'x', 'Color', 'k', 'DisplayName', 'intersects');

for k = 1:(j-1)
    text(intersectsx(k), intersectsy(k), ['\leftarrow (', num2str(intersectsx(k)), ', ', num2str(intersectsy(k)), ')']);
end

xlabel('F_B_,_v / F_z');
ylabel('F_B_,_h / F_z');
title('Distribution');
legend();
hold off;

function [FBh_over_Fz] = real_breaking_dist(FBv_over_Fz)
    m = 0.6559;
    
    FBh_over_Fz = m .* FBv_over_Fz;
end

function [FBh_over_Fz] = ideal_breaking_dist(FBv_over_Fz)
    lh = 2;
    h = 1.75;
    l = 5;
    
    FBh_over_Fz = -(lh/(2*h)) + sqrt((lh/(2*h))^2 + FBv_over_Fz * (l/h)) - FBv_over_Fz;
end
