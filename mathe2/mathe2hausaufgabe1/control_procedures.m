% Clear the workspace at the beginning of the execution of a program. 
% This should be performed, if a program was running before in the 
% session or in general at the beginning of every newly performed program.
clear;
% Close all opened plots. This is useful, if a plot was opened before this 
% program was started.
close all;

% Perform the function defined beneath. The function compute an equidistant 
% array of point, where 0 is the start of the interval, xend is the end of 
% the interval and 100 is the number of equidistant points.
N=100;
xstart=0;
xend=1;
[xfor,Deltaxfor]=equidistant_array_for(xstart,xend,N);
[xwhile,Deltaxwhile]=equidistant_array_while(xstart,xend,N);

% Check by use of an if condition, whether the size of the compute array is 
% suited to the user-defined size of N.
if(max(size(xfor))~=N || max(size(xwhile))~=N)
    disp('Error: The size of the array xfor or the array xwhile is not equal to N!');
else
    disp('The sizes of the arrays xfor and xwhile are equal to N.')
end

% Definition of two functions which performs an equidistant partition 
% of N points within an interval [xstart,xend] by use of a for or a 
% while loop. The result is the arrays of points x and the distance 
% between two points.
function [x,Deltax] = equidistant_array_for(xstart,xend,N)
    % Compute the distance between two points of the arrays x.
    Deltax=(xend-xstart)/N;

    % Perform a for loop, to compute all entries of the array x.
    for i=1:N
        x(i)=xstart+(i-0.5)*Deltax;
    end
end
function [x,Deltax] = equidistant_array_while(xstart,xend,N)
    % Compute the distance between two points of the arrays x.
    Deltax=(xend-xstart)/N;

    % Perform a while loop, to compute all entries of the array x.
    i=1;
    while(i<=N)
        x(i)=xstart+(i-0.5)*Deltax;
        i=i+1;
    end
end
