a = fakultaet_f(5) %#ok<NOPTS>
b = fakultaet_w(5) %#ok<NOPTS>
c = fakultaet_r(5) %#ok<NOPTS>

function [fak_n_f]=fakultaet_f(n)
  fak_n_f = 1;
  for i = 1:n
    fak_n_f = fak_n_f * i;
  end
end

function [fak_n_w]=fakultaet_w(n)
  fak_n_w = 1;
  i = 1;
  while i <= n
    fak_n_w = fak_n_w * i;
    i = i + 1;
  end
end

function [fak_n_r]=fakultaet_r(n)
  if n > 1
    fak_n_r = n * fakultaet_r(n-1);
  else
    fak_n_r = 1;
  end
end