d = 0:pi/1000:2*pi;
x = cos(d);
y = sin(d);

plot (x,y,'black');
xlabel('x');
ylabel('y');
title('Circle');

axis equal;