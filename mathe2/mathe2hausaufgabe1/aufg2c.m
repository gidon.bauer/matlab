x = 0:1/100000:0.1;
N = 5; % Grenzwert von n_k. Wenn n_k konvergiert l�uft die Teilfolge gegen 0

plot(x, f(N, x), 'black');
disp(f(N, 0));

function [y]=f(n,x)
    y = sin(n.*pi.*x);
end
