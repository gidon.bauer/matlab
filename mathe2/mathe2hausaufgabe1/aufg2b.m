x = 0:1/10000000:1;
n = 1000;

% hold on;
% for a = 0:5
%   plot(x,f(a,x),'color',rand(1,3));
% end
% hold off;

plot(x,f(n,x),'black');

function [y]=f(n,x)
  y = sin(n*pi*x);
end