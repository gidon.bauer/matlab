%% Mathe II Programieraufgabe 3
%% Michel Mantsch, Jan Liebehenschel, Gidon Bauer

%% Workspace leeren
clear;
clc;

%% Auswahl der Aufgabe
choice = 1;

%% Aufgabenteil a)
if choice == 1
    A = [3,0,0;
         4,0,5;
         0,3,-2;
         0,4,4];

     b = [5;
         0;
         1;
         -2];

    [Q_h,R_h] = hholder(A);
    x_h = solve_QR(Q_h,R_h,b);
    r_h = residuum(A,x_h,b);
    
    [Q_g,R_g] = gschmidt(A);
    x_g = solve_QR(Q_g,R_g,b);
    r_g = residuum(A,x_g,b);
    
%% Aufgabenteil b)
elseif choice == 2
    eps = 1e-2;

    A = [1,1,1;
         eps,0,0;
         0,eps,0;
         0,0,eps];

    b = ones(4,1);
    
    [Q_h,R_h] = hholder(A);
    I_h = Q_h' * Q_h;
%     disp(abs(eye(3)-I_h));

    [Q_g,R_g] = gschmidt(A);
    I_g = Q_g' * Q_g;
%     disp(abs(eye(3)-I_g));

%% Aufgabenteil c)
elseif choice == 3
    n = 6;
    A = hilb(n);
    b = ones(n,1);
    
    cond(A)
    
    [Q_h,R_h] = hholder(A);
    x_h = solve_QR(Q_h,R_h,b);
    r_h = residuum(A,x_h,b);
    I_h = Q_h' * Q_h;

    [Q_g,R_g] = gschmidt(A);
    x_g = solve_QR(Q_g,R_g,b);
    r_g = residuum(A,x_g,b);
    I_g = Q_g' * Q_g;
    
    clear n; 
end

clear choice;

%% Householder Verfahren
function [Q,R] = hholder(A)
    %% Deklaration von R & Q
    [m,n] = size(A);
    Q = eye(m);
    %% Berechnung von R & Q, Anfangswerte
    x = A(:,1);
    alpha = sign(x(1)) * norm(x);
    v = x - alpha * einh(m,1);
    Q_temp = eye(m) - 2 * (v * v')/(v' * v);
    A = Q_temp * A;
    Q = Q * Q_temp';
    %% Berchnung von R & Q, Iteration
    for i = 2:n
        x = A(:,i);
        v = zeros(m,1);
        alpha = norm(x(i:m,1));
        v(i:m,1) = x(i:m,1) - alpha * einh((m-i+1),1);
        Q_temp = eye(m) - 2 * (v * v')/(v' * v);
        A = Q_temp * A;
        Q = Q * Q_temp';
    end
    R = A(1:n,1:n);
    Q = Q(1:m,1:n);
end

%% Gram-Schmidt Verfahren
function [Q,R] = gschmidt(A)
    %% Deklaration von R & Q
    [m,n] = size(A);
    R = zeros(n);
    Q = zeros(m,n);
    %% Berchnung von R & Q, Anfangswerte
    R(1,1) = norm(A(:,1));
    Q(:,1) = A(:,1)/R(1,1);
    %% Berchnung von R & Q, Iteration
    for k = 2:n
        sum = zeros(m,1);
        for i = 1:(k-1)
            R(i,k) = Q(:,i).' * A(:,k);
            sum = sum + R(i,k)*Q(:,i);
        end
        b = A(:,k) - sum;
        R(k,k) = norm(b);
        Q(:,k) = b/R(k,k);
    end
end

%% Erzeugt den i-ten Einheitsvektor
function [e] = einh(m,i)
    if m >= i
        e = zeros(m,1);
        e(i,1) = 1;
    end
end

%% L�st LGS mit QR Zerlegung
function [x] = solve_QR(Q,R,b)
    y = Q' * b;
    s = size(R);
    si = s(1);
    x = zeros(si,1);
    for k = 0:(si-1)
        i = si-k;
        sum = 0;
        for j = (i+1):si
            sum = sum + R(i,j) * x(j);
        end
        x(i) = (y(i) - sum)/R(i,i); 
    end
end

%% Berechnet das Residuum
function [r] = residuum(A,x,b)
    r = norm(A*x - b);
end