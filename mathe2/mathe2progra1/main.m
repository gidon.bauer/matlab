%% Mathe II: Erste Programmieraufgabe
%% Gidon Bauer, Michel Mantsch, Jan Liebehenschel

%% Deklaration Variablen
func = @h;
left_border = -2;
right_border = 3;
tolerance = 1e-7;
N = 10;

%% Funktionsaufruf Fehlertoleranz
F_tol = gaussq_tol(func,left_border,right_border,tolerance);
disp(F_tol);

%% Funktionsaufruf Anzahl Stuetzstellen
F_n = gaussq_n(func,left_border,right_border,N);
disp(F_n);

%% Funktionen
function [y] = f(x)
    y = x^10;
end

function [y] = g(x)
    y = sin(x);
end

function [y] = h(x)
    y = 1/(1e-2 + x^2);
end

%% Gauss Quadratur mit gegebener Anzahl an Stuetzstellen
function [int] = gaussq_n(f,a,b,n)
    %% Stuetzstellen
    A = zeros(n);
    for k = 1:n
        if (k+1)<=n
            beta = k/sqrt(4*k^2 - 1);
            A(k, k+1) = beta;
            A(k+1, k) = beta;
        end
    end
    [Z,x_untransformed] = eig(A);
    x = zeros(1,n);
    for k = 1:n
        x(k) = (x_untransformed(k,k)+1)*(b - a)/2 + a;
    end
    %% Gewichte
    omega = zeros(1,n);
    for k = 1:n
        weight_untransformed = 2 * Z(1,k)^2;
        omega(k) = (b-a)/2 * weight_untransformed;
    end
    %% Loesung
    int = 0;
    for k = 1:n
        int = int + omega(k) * f(x(k)); 
    end
end

%% Gauss Quadratur mit gegebener Toleranz
function [int] = gaussq_tol(f,a,b,tol)
    n = 1;
    eps = inf;
    while eps >= tol
        eps = abs(gaussq_n(f,a,b,n+1) - gaussq_n(f,a,b,n));
        n = n+1
    end
    int = gaussq_n(f,a,b,n);
end