%% Mathe II: Zweite Programmieraufgabe
%% Gidon Bauer, Michel Mantsch, Jan Liebehenschel

clear;
clc;

%% Testen der Funktionen
choice = 1; % Wahl welcher Aufgabenteil getestet werden soll.

if choice == 1
    %% Aufgabenteil a)
    A = [1,1,3;
           1,2,2;
           2,1,5];

    b = [2;
           1;
           1];

    [L_ohne,R_ohne] = lr_ohne(A);
    [L_mit,R_mit,P] = lr_mit(A);

    x_ohne = LGS_LR(L_ohne,R_ohne,b);
    x_mit = LGS_LRP(L_mit,R_mit,P,b);
    
elseif choice == 2
    %% Aufgabenteil b)
    n = 6;
    A = zeros(n);
    for i = 1:n
        for j = 1:n
            A(i,n-j+1) = 3^(-abs(i-j)) + 2^(-j-i) + (10^(-10) * randn);
        end
    end
    
    [L_ohne,R_ohne] = lr_ohne(A);  % Teilweise sehr gro�e, bzw. kleine Werte
    [L_mit,R_mit,P] = lr_mit(A);   % Werte in �hnlichen Gr��enverh�ltnissen
    
    clear i; clear j; clear n;
    
elseif choice == 3
    %% Aufgabenteil c)
    A = [0,1,0,-1;
         1,2,2,1;
         1,1,1,1;
         2,1,-1,2];

    b = [2;
        -1;
        -2;
        -11];

    [L_ohne,R_ohne] = lr_ohne(A);
    [L_mit,R_mit,P] = lr_mit(A);

    x_ohne = LGS_LR(L_ohne,R_ohne,b);    % Keine L�sung, da es keine LR Zerlegung gibt.
    x_mit = LGS_LRP(L_mit,R_mit,P,b);  % Stimmt exakt mit der Matlab L�sung �berein.

    x_test = A\b;
    
elseif choice == 4
    %% Aufgabenteil d)
    n = 4;
    H = zeros(n);
    for i = 1:n
        for j = 1:n
            H(i,j) = 1/(i+j-1);
        end
    end
    x = ones(n,1);
    b = H * x;
    
    [L_ohne,R_ohne] = lr_ohne(H);
    [L_mit,R_mit,P] = lr_mit(H);
    
    y_ohne = LGS_LR(L_ohne,R_ohne,b); % Stimmt mit x �berein f�r n < 10, danach ist es dennoch nah an x
    y_mit = LGS_LRP(L_mit,R_mit,P,b); % Stimmt mit x und dem Test f�r n <= 2 �berein, sonst nicht; wird sehr gro�
    
    y_test = H\b;
    
    clear i; clear j; clear n;

end

clear choice;


%% Berechnet die L�sung eines LGS aus LR
function [x] = LGS_LR(L,R,b)
    %% Bestimmung der Gr��e
    s = size(L);
    si = s(1);
    %% Dekralration von y
    y = zeros(si,1);
    %% Berechnung von y
    for i = 1:si
        temp = 0;
        for j = 1:(i-1)
            temp = temp + L(i,j) * y(j);
        end
        y(i) = (b(i) - temp)/L(i,i);
    end
    %% Deklaration von x
    x = zeros(si,1);
    %% Berechnung von x
    for k = 0:(si-1)
        i = si-k;
        sum = 0;
        for j = (i+1):si
            sum = sum + R(i,j) * x(j);
        end
        x(i) = (y(i) - sum)/R(i,i); 
    end
end

%% Berechnet die L�sung eines LGS aus LRP
function [x] = LGS_LRP(L,R,P,b)
    %% Bestimmung der Gr��e
    s = size(L);
    si = s(1);
    %% Berechnung von z
    z = P * b;
    %% Dekralration von y
    y = zeros(si,1);
    %% Berechnung von y
    for i = 1:si
        temp = 0;
        for j = 1:(i-1)
            temp = temp + L(i,j) * y(j);
        end
        y(i) = (z(i) - temp)/L(i,i);
    end
    %% Deklaration von x
    x = zeros(si,1);
    %% Berechnung von x
    for k = 0:(si-1)
        i = si-k;
        sum = 0;
        for j = (i+1):si
            sum = sum + R(i,j) * x(j);
        end
        x(i) = (y(i) - sum)/R(i,i); 
    end
end

%% LR-Zerlegung ohne Pivotisierung
function [L,R] = lr_ohne(A)
    %% �berpr�fung ob A korrekte Dimensionen hat.
    s = size(A);
    if s(1) ~= s(2)
        L = [];
        R = [];
        return
    end
    si = s(1);
    %% Deklaration von L und R
    L = eye(si);
    R = A;
    %% Berechnung von L und R
    for i = 1:si
        for j = (i+1):si
            % Abbruch falls eines der Dimensionalelemente Null ist.
            if R(i,i) == 0
                L = [];
                R = [];
                return
            end
            % --
            L(j,i) = R(j,i)/R(i,i);
            R(j,:) = R(j,:) - (L(j,i) .* R(i,:));
        end
    end
end

%% LR-Zerlegung mit Pivotisierung
function [L,R,P] = lr_mit(A)
    %% �berpr�fung ob A korrekte Dimensionen hat.
    s = size(A);
    if s(1) ~= s(2)
        L = [];
        R = [];
        P = [];
        return
    end
    si = s(1);
    %% Deklaration von L, R und P
    L = eye(si);
    R = A;
    P = eye(si);
    %% Berechnung von L, R und P
    for i = 1:si
        piv_col = find_max(R(i:si,i)) + (i-1);
        R = swap(R,i,piv_col);
        P = swap(P,i,piv_col);
        for j = (i+1):si
            L(j,i) = R(j,i)/R(i,i);
            R(j,:) = R(j,:) - (L(j,i) .* R(i,:));
        end
    end
end

%% Findet das gr��te Element eines Vektors
function [max_pos] = find_max(vec)
    max_pos = 1;
    for i = 2:size(vec)
        if (vec(max_pos) < vec(i))
            max_pos = i;
        end
    end
end

%% Vertauscht zwei Zeilen einer Matrix
function [Mat] = swap(Mat, i, j)
    temp = Mat(i,:);
    Mat(i,:) = Mat(j,:);
    Mat(j,:) = temp;
end