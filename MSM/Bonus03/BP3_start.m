% Backward heat equation in 1D
% Bonus problem 3: iterative regularization + parameter choice
% 
% Stefan Basermann & Gidon Bauer

close all;
clear;
clc;

L = pi;       % length of the rod
T = 5;        % final time
sigma = 1e-2; % magnitude of error
kappa = 1e-3; % heat conduction coefficient
n = 100;
N = n+1;      % number of intervals
h = L/N;      % grid size

% create C
C = zeros(n,n);
% first and last row
C(1,1:2) = [2 -1];
C(n,n-1:n) = [-1 2];
% inner rows
for i = 2:n-1
    C(i,i-1:i+1) = [-1 2 -1];
end
% and, finally, multiply with the right factor
C = C*(kappa/h^2);

%% backward heat conduction
x = linspace(h,L-h,n)';       % generate linearly spaced vector
u_0 = sin(x) + 0.5*sin(4*x);  % initial temperature
A = expm(-T*C);
u_f = A*u_0;                  % final temperature
load error.mat;               % error (random noise)
u_f_meas = u_f + error;       % measurement of final temperature
u_0_est = expm(T*C)*u_f_meas;

sprintf('1d backward heat equation with n=%i and noise level sigma=%g\n==============================================================', n, sigma);
  
err = norm(u_0 - u_0_est);

figure(1);
subplot(1,2,1);
plot(x, u_f, x, u_f_meas, x, A*u_0_est);
legend('u_f', 'u_f measured', 'u_f est. (data fit)');
subplot(1,2,2);
plot(x, u_0, x, u_0_est, 'r');
legend('u_0', 'estimation for u_0');

% Landweber iterations
max_iter = 250;

iter = zeros(n,max_iter); % store all iterates

s = svd(A);     % singular values of A

beta = 1;
if (beta <= 0 || beta >= s(1)^-2)
    warning('Beta hat einen falschen Wert.'); % check if beta has a correct value (a)
end

k_star = -1;        % stopping index
eps_alpha = 1e-01;

xi_norm = zeros(max_iter-1, 1); 
C_xi_norm = zeros(max_iter-1, 1);
discrepancy = zeros(max_iter-1, 1);

for i = 2:max_iter
    iter(:,i) = iter(:,i-1) - beta * A' * (A * iter(:,i-1) - u_f_meas);
    % c)?
    xi_norm(i-1) = norm(iter(:,i), 2);
    discrepancy(i-1) = norm(A*iter(:,i) - u_f_meas, 2);
    % d)
    C_xi_norm(i-1) = norm(C*iter(:,i), 2);
    % b)
    if discrepancy(i-1) <= eps_alpha && k_star == -1
        k_star = i;
        % Ausgabe von k*
        disp(['k* = ', num2str(k_star)]);
        % Ausgabe der Discrepancy
        disp(['||A*iter(:,', num2str(k_star),  ') - u_f_meas||_2 = ',...
              num2str(discrepancy(i-1)), ' < ', num2str(eps_alpha), ...
              ' = eps_alpha']);
    end
end

% b)
figure(2);
plot(x, u_0, 'DisplayName', 'exact');
hold on;
plot(x, iter(:, k_star), 'DisplayName', ['estimate for k* = ', num2str(k_star)]);
plot(x, iter(:, end), 'DisplayName', ['estimate for k = ', num2str(max_iter)]);
xlabel('x');
ylabel('temperature');
legend;
hold off;

% b)
figure(3);
plot(x, u_f, 'DisplayName', 'exact');
hold on;
plot(x, u_f_meas, 'DisplayName', 'measurment');
plot(x, A*iter(:, k_star), 'DisplayName', '$\bar{y}$');
xlabel('x');
ylabel('temperature');
legend('Interpreter', 'latex');
hold off;

startingpoint = 1;
figure(4);
loglog(discrepancy(startingpoint:end), xi_norm(startingpoint:end), '.',...
       'DisplayName', 'Discrepancy');
hold on;
xlabel('$||A \tilde{\xi_k} - \tilde{y}||_2$', 'Interpreter', 'latex');
ylabel('$||\tilde{\xi_k}||_2$', 'Interpreter', 'latex');
title('L curve');
legend;
hold off;

figure(5);
loglog(discrepancy, C_xi_norm, '.', 'DisplayName', 'Discrepancy');
hold on;
xlabel('$||A \tilde{\xi_k} - \tilde{y}||_2$', 'Interpreter', 'latex');
ylabel('$||C \tilde{\xi_k}||_2$', 'Interpreter', 'latex');
title('L curve');
legend;
hold off;