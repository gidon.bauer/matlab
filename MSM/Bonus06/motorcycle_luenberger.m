function x_dot = motorcycle_luenberger(t, x)
    load('y_meas.mat', 'y_meas');
    global pol_param;
    
    A = [0 1;0 0];
    B = [0;1];
    C = [1 0];

    K = place(A', C', [pol_param + 1i, pol_param - 1i]);
    K = K';

    a = @(t) 0.001 * sin(pi/10 * t) * (t - 40)^2;

    x_dot = (A - K*C)*x + B * a(t) + K * y_meas(t);
end