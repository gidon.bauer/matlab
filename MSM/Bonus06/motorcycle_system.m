function x_dot = motorcycle_system(t, x)
    A = [0 1;0 0];
    B = [0;1];

    a = @(t) 0.001 * sin(pi/10 * t) * (t - 40)^2;

    x_dot = A * x + B * a(t);
end